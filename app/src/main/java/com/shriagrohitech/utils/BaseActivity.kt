package com.shriagrohitech.utils

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.shriagrohitech.retrofit.RetrofitApiClient
import com.shriagrohitech.retrofit.RetrofitService


open class BaseActivity : AppCompatActivity() {

    var retrofitApiClient: RetrofitApiClient? = null
    var cd: ConnectionDetector? = null
    var mContext: Context? = null

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()
        /*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }*/
    }

    fun showHomeBackOnToolbar(toolbar: Toolbar?) {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun showToast(ctx: Context, msg: String){
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
    }

}

