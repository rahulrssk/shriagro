package com.shriagrohitech.utils.payumoney;

/**
 * Created by Rahul Hooda on 14/7/17.
 */
public enum AppEnvironment {

    //SandBox By PayuMoney

    SANDBOX {

        @Override
        public String merchant_Key() {
            return "QylhKRVd";
        }

        @Override
        public String merchant_ID() {
            return "5960507";
        }

        @Override
        public String furl() {
            return "https://www.payumoney.com/mobileapp/payumoney/failure.php";
        }

        @Override
        public String surl() {
            return "https://www.payumoney.com/mobileapp/payumoney/success.php";
        }

        @Override
        public String salt() {
            return "seVTUgzrgE";
        }

        @Override
        public boolean debug() {
            return true;
        }
    },

    PRODUCTION {

        @Override
        public String merchant_Key() {
            return "M6T8XhdT";
        }

        @Override
        public String merchant_ID() {
            return "7327539";
        }

        @Override
        public String furl() {
            return "https://www.payumoney.com/mobileapp/payumoney/failure.php";
        }

        @Override
        public String surl() {
            return "https://www.payumoney.com/mobileapp/payumoney/success.php";
        }

        @Override
        public String salt() {
            return "niint5Hzu8";
        }

        @Override
        public boolean debug() {
            return false;
        }
    };

    public abstract String merchant_Key();

    public abstract String merchant_ID();

    public abstract String furl();

    public abstract String surl();

    public abstract String salt();

    public abstract boolean debug();


}
