package com.shriagrohitech.utils;

import android.app.Dialog;
import android.view.View;
import android.view.Window;

import com.google.android.material.snackbar.Snackbar;
import com.shriagrohitech.R;

public class AppProgressDialog {


    public static void snack(View container, String msg, String title, View.OnClickListener listener) {
        Snackbar snackbar = Snackbar
                .make(container, msg, Snackbar.LENGTH_INDEFINITE)
                .setAction(title, listener);
        snackbar.show();
    }

    public static void show(Dialog mProgressDialog) {
        try {
            if (mProgressDialog.isShowing())
                return;
            mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mProgressDialog.setContentView(R.layout.layout_progress_bar);
            // ((TextView) mProgressDialog.findViewById(R.id.title)).setText(msg);
            mProgressDialog.setCancelable(false);
            if (mProgressDialog.getWindow() != null)
                mProgressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            mProgressDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hide(Dialog mProgressDialog) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}