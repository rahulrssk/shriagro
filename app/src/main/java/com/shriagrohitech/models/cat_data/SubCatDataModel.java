package com.shriagrohitech.models.cat_data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCatDataModel implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("product_data")
    @Expose
    private List<ProductDatum> productData = null;
    public final static Creator<SubCatDataModel> CREATOR = new Creator<SubCatDataModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SubCatDataModel createFromParcel(Parcel in) {
            return new SubCatDataModel(in);
        }

        public SubCatDataModel[] newArray(int size) {
            return (new SubCatDataModel[size]);
        }

    }
            ;

    protected SubCatDataModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.productData, (ProductDatum.class.getClassLoader()));
    }

    public SubCatDataModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductDatum> getProductData() {
        return productData;
    }

    public void setProductData(List<ProductDatum> productData) {
        this.productData = productData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(productData);
    }

    public int describeContents() {
        return 0;
    }

}