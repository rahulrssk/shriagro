package com.shriagrohitech.models.cat_data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryDataModel implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("sub_category")
    @Expose
    private List<SubCategory> subCategory = null;
    @SerializedName("product_data")
    @Expose
    private List<ProductDatum> productData = null;
    public final static Creator<CategoryDataModel> CREATOR = new Creator<CategoryDataModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CategoryDataModel createFromParcel(Parcel in) {
            return new CategoryDataModel(in);
        }

        public CategoryDataModel[] newArray(int size) {
            return (new CategoryDataModel[size]);
        }

    }
            ;

    protected CategoryDataModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.subCategory, (SubCategory.class.getClassLoader()));
        in.readList(this.productData, (ProductDatum.class.getClassLoader()));
    }

    public CategoryDataModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubCategory> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(List<SubCategory> subCategory) {
        this.subCategory = subCategory;
    }

    public List<ProductDatum> getProductData() {
        return productData;
    }

    public void setProductData(List<ProductDatum> productData) {
        this.productData = productData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(subCategory);
        dest.writeList(productData);
    }

    public int describeContents() {
        return 0;
    }

}