package com.shriagrohitech.models.cat_data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDatum implements Parcelable {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("product_main_cat")
    @Expose
    private String productMainCat;
    @SerializedName("product_sub_cat")
    @Expose
    private String productSubCat;
    @SerializedName("product_description")
    @Expose
    private String productDescription;
    @SerializedName("product_variation")
    @Expose
    private List<ProductVariation> productVariation = null;
    public final static Creator<ProductDatum> CREATOR = new Creator<ProductDatum>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProductDatum createFromParcel(Parcel in) {
            return new ProductDatum(in);
        }

        public ProductDatum[] newArray(int size) {
            return (new ProductDatum[size]);
        }

    };

    protected ProductDatum(Parcel in) {
        this.productId = ((String) in.readValue((String.class.getClassLoader())));
        this.productName = ((String) in.readValue((String.class.getClassLoader())));
        this.productImage = ((String) in.readValue((String.class.getClassLoader())));
        this.productMainCat = ((String) in.readValue((String.class.getClassLoader())));
        this.productSubCat = ((String) in.readValue((String.class.getClassLoader())));
        this.productDescription = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.productVariation, (ProductVariation.class.getClassLoader()));
    }

    public ProductDatum() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductMainCat() {
        return productMainCat;
    }

    public void setProductMainCat(String productMainCat) {
        this.productMainCat = productMainCat;
    }

    public String getProductSubCat() {
        return productSubCat;
    }

    public void setProductSubCat(String productSubCat) {
        this.productSubCat = productSubCat;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public List<ProductVariation> getProductVariation() {
        return productVariation;
    }

    public void setProductVariation(List<ProductVariation> productVariation) {
        this.productVariation = productVariation;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(productId);
        dest.writeValue(productName);
        dest.writeValue(productImage);
        dest.writeValue(productMainCat);
        dest.writeValue(productSubCat);
        dest.writeValue(productDescription);
        dest.writeList(productVariation);
    }

    public int describeContents() {
        return 0;
    }

}