package com.shriagrohitech.models.cat_data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductVariation implements Parcelable {

    @SerializedName("variation_id")
    @Expose
    private String variationId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("selected")
    @Expose
    private boolean selected;
    @SerializedName("stock_price")
    @Expose
    private String stockPrice;
    @SerializedName("stock_mrp")
    @Expose
    private String stockMrp;
    @SerializedName("variation")
    @Expose
    private String variation;
    @SerializedName("variation_quantity")
    @Expose
    private String variationQuantity;
    @SerializedName("stock")
    @Expose
    private String stock;
    @SerializedName("cart_id")
    @Expose
    private String cartId;
    @SerializedName("cart_quantity")
    @Expose
    private String cartQuantity;
    @SerializedName("cart_data")
    @Expose
    private Integer cartData;
    public final static Creator<ProductVariation> CREATOR = new Creator<ProductVariation>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProductVariation createFromParcel(Parcel in) {
            return new ProductVariation(in);
        }

        public ProductVariation[] newArray(int size) {
            return (new ProductVariation[size]);
        }

    };

    protected ProductVariation(Parcel in) {
        this.variationId = ((String) in.readValue((String.class.getClassLoader())));
        this.productId = ((String) in.readValue((String.class.getClassLoader())));
        this.selected = ((boolean) in.readValue((String.class.getClassLoader())));
        this.stockPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.stockMrp = ((String) in.readValue((String.class.getClassLoader())));
        this.variation = ((String) in.readValue((String.class.getClassLoader())));
        this.variationQuantity = ((String) in.readValue((String.class.getClassLoader())));
        this.stock = ((String) in.readValue((String.class.getClassLoader())));
        this.cartId = ((String) in.readValue((String.class.getClassLoader())));
        this.cartQuantity = ((String) in.readValue((String.class.getClassLoader())));
        this.cartData = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public ProductVariation() {
    }

    public String getVariationId() {
        return variationId;
    }

    public void setVariationId(String variationId) {
        this.variationId = variationId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(String stockPrice) {
        this.stockPrice = stockPrice;
    }

    public String getStockMrp() {
        return stockMrp;
    }

    public void setStockMrp(String stockMrp) {
        this.stockMrp = stockMrp;
    }

    public String getVariation() {
        return variation;
    }

    public void setVariation(String variation) {
        this.variation = variation;
    }

    public String getVariationQuantity() {
        return variationQuantity;
    }

    public void setVariationQuantity(String variationQuantity) {
        this.variationQuantity = variationQuantity;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getCartQuantity() {
        return cartQuantity;
    }

    public void setCartQuantity(String cartQuantity) {
        this.cartQuantity = cartQuantity;
    }

    public Integer getCartData() {
        return cartData;
    }

    public void setCartData(Integer cartData) {
        this.cartData = cartData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(variationId);
        dest.writeValue(productId);
        dest.writeValue(selected);
        dest.writeValue(stockPrice);
        dest.writeValue(stockMrp);
        dest.writeValue(variation);
        dest.writeValue(variationQuantity);
        dest.writeValue(stock);
        dest.writeValue(cartId);
        dest.writeValue(cartQuantity);
        dest.writeValue(cartData);
    }

    public int describeContents() {
        return 0;
    }

}