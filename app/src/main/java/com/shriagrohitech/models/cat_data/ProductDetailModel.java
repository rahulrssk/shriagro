package com.shriagrohitech.models.cat_data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductDetailModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("product_data")
    @Expose
    private ProductDatum productData;
    @SerializedName("related_product")
    @Expose
    private List<ProductDatum> relatedProduct = null;
    public final static Creator<ProductDetailModel> CREATOR = new Creator<ProductDetailModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProductDetailModel createFromParcel(Parcel in) {
            return new ProductDetailModel(in);
        }

        public ProductDetailModel[] newArray(int size) {
            return (new ProductDetailModel[size]);
        }

    };

    protected ProductDetailModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.productData = ((ProductDatum) in.readValue((ProductDatum.class.getClassLoader())));
        in.readList(this.relatedProduct, (ProductDatum.class.getClassLoader()));
    }

    public ProductDetailModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProductDatum getProductData() {
        return productData;
    }

    public void setProductData(ProductDatum productData) {
        this.productData = productData;
    }

    public List<ProductDatum> getRelatedProduct() {
        return relatedProduct;
    }

    public void setRelatedProduct(List<ProductDatum> relatedProduct) {
        this.relatedProduct = relatedProduct;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(productData);
        dest.writeList(relatedProduct);
    }

    public int describeContents() {
        return 0;
    }

}