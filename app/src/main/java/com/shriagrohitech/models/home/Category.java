package com.shriagrohitech.models.home;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category implements Parcelable {

    @SerializedName("main_cat_id")
    @Expose
    private String mainCatId;
    @SerializedName("main_cat_name")
    @Expose
    private String mainCatName;
    @SerializedName("main_cat_image")
    @Expose
    private String mainCatImage;
    @SerializedName("sub_category")
    @Expose
    private List<SubCategory> subCategory = new ArrayList();
    public final static Parcelable.Creator<Category> CREATOR = new Creator<Category>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        public Category[] newArray(int size) {
            return (new Category[size]);
        }

    };

    protected Category(Parcel in) {
        this.mainCatId = ((String) in.readValue((String.class.getClassLoader())));
        this.mainCatName = ((String) in.readValue((String.class.getClassLoader())));
        this.mainCatImage = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.subCategory, (com.shriagrohitech.models.home.SubCategory.class.getClassLoader()));
    }

    public Category() {
    }

    public String getMainCatId() {
        return mainCatId;
    }

    public void setMainCatId(String mainCatId) {
        this.mainCatId = mainCatId;
    }

    public String getMainCatName() {
        return mainCatName;
    }

    public void setMainCatName(String mainCatName) {
        this.mainCatName = mainCatName;
    }

    public String getMainCatImage() {
        return mainCatImage;
    }

    public void setMainCatImage(String mainCatImage) {
        this.mainCatImage = mainCatImage;
    }

    public List<SubCategory> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(List<SubCategory> subCategory) {
        this.subCategory = subCategory;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(mainCatId);
        dest.writeValue(mainCatName);
        dest.writeValue(mainCatImage);
        dest.writeList(subCategory);
    }

    public int describeContents() {
        return 0;
    }

}