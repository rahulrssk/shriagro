package com.shriagrohitech.models.home;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.shriagrohitech.models.cat_data.ProductDatum;

public class HomeModel implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cart_data")
    @Expose
    private Integer cartData;
    @SerializedName("banner_data")
    @Expose
    private List<BannerDatum> bannerData = null;
    @SerializedName("category")
    @Expose
    private List<Category> category = null;
    @SerializedName("slot_data")
    @Expose
    private SlotData slotData;
    @SerializedName("promo_data")
    @Expose
    private List<PromoDatum> promoData = null;
    @SerializedName("blog")
    @Expose
    private List<Blog> blog = null;
    @SerializedName("tranding_product")
    @Expose
    private List<ProductDatum> trandingProduct = null;
    public final static Parcelable.Creator<HomeModel> CREATOR = new Creator<HomeModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HomeModel createFromParcel(Parcel in) {
            return new HomeModel(in);
        }

        public HomeModel[] newArray(int size) {
            return (new HomeModel[size]);
        }

    }
            ;

    protected HomeModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.cartData = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.bannerData, (com.shriagrohitech.models.home.BannerDatum.class.getClassLoader()));
        in.readList(this.category, (com.shriagrohitech.models.home.Category.class.getClassLoader()));
        this.slotData = ((SlotData) in.readValue((SlotData.class.getClassLoader())));
        in.readList(this.promoData, (com.shriagrohitech.models.home.PromoDatum.class.getClassLoader()));
        in.readList(this.blog, (com.shriagrohitech.models.home.Blog.class.getClassLoader()));
        in.readList(this.trandingProduct, (ProductDatum.class.getClassLoader()));
    }

    public HomeModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCartData() {
        return cartData;
    }

    public void setCartData(Integer cartData) {
        this.cartData = cartData;
    }

    public List<BannerDatum> getBannerData() {
        return bannerData;
    }

    public void setBannerData(List<BannerDatum> bannerData) {
        this.bannerData = bannerData;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public SlotData getSlotData() {
        return slotData;
    }

    public void setSlotData(SlotData slotData) {
        this.slotData = slotData;
    }

    public List<PromoDatum> getPromoData() {
        return promoData;
    }

    public void setPromoData(List<PromoDatum> promoData) {
        this.promoData = promoData;
    }

    public List<Blog> getBlog() {
        return blog;
    }

    public void setBlog(List<Blog> blog) {
        this.blog = blog;
    }

    public List<ProductDatum> getTrandingProduct() {
        return trandingProduct;
    }

    public void setTrandingProduct(List<ProductDatum> trandingProduct) {
        this.trandingProduct = trandingProduct;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(cartData);
        dest.writeList(bannerData);
        dest.writeList(category);
        dest.writeValue(slotData);
        dest.writeList(promoData);
        dest.writeList(blog);
        dest.writeList(trandingProduct);
    }

    public int describeContents() {
        return 0;
    }

}