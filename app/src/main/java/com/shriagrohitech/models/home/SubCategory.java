package com.shriagrohitech.models.home;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategory implements Parcelable {

    @SerializedName("sub_cat_id")
    @Expose
    private String subCatId;
    @SerializedName("main_cat_id")
    @Expose
    private String mainCatId;
    @SerializedName("sub_cat_name")
    @Expose
    private String subCatName;
    public final static Parcelable.Creator<SubCategory> CREATOR = new Creator<SubCategory>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        public SubCategory[] newArray(int size) {
            return (new SubCategory[size]);
        }

    };

    protected SubCategory(Parcel in) {
        this.subCatId = ((String) in.readValue((String.class.getClassLoader())));
        this.mainCatId = ((String) in.readValue((String.class.getClassLoader())));
        this.subCatName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public SubCategory() {
    }

    public String getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(String subCatId) {
        this.subCatId = subCatId;
    }

    public String getMainCatId() {
        return mainCatId;
    }

    public void setMainCatId(String mainCatId) {
        this.mainCatId = mainCatId;
    }

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(subCatId);
        dest.writeValue(mainCatId);
        dest.writeValue(subCatName);
    }

    public int describeContents() {
        return 0;
    }

}