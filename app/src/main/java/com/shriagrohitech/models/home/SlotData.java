package com.shriagrohitech.models.home;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SlotData implements Parcelable {

    @SerializedName("slot_id")
    @Expose
    private String slotId;
    @SerializedName("time_slot")
    @Expose
    private String timeSlot;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("slot_status")
    @Expose
    private String slotStatus;
    public final static Parcelable.Creator<SlotData> CREATOR = new Creator<SlotData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SlotData createFromParcel(Parcel in) {
            return new SlotData(in);
        }

        public SlotData[] newArray(int size) {
            return (new SlotData[size]);
        }

    };

    protected SlotData(Parcel in) {
        this.slotId = ((String) in.readValue((String.class.getClassLoader())));
        this.timeSlot = ((String) in.readValue((String.class.getClassLoader())));
        this.startTime = ((String) in.readValue((String.class.getClassLoader())));
        this.endTime = ((String) in.readValue((String.class.getClassLoader())));
        this.slotStatus = ((String) in.readValue((String.class.getClassLoader())));
    }

    public SlotData() {
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSlotStatus() {
        return slotStatus;
    }

    public void setSlotStatus(String slotStatus) {
        this.slotStatus = slotStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(slotId);
        dest.writeValue(timeSlot);
        dest.writeValue(startTime);
        dest.writeValue(endTime);
        dest.writeValue(slotStatus);
    }

    public int describeContents() {
        return 0;
    }

}