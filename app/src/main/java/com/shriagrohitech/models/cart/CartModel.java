package com.shriagrohitech.models.cart;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("delivery_charges")
    @Expose
    private String deliveryCharges;
    @SerializedName("minimum_purchase")
    @Expose
    private String minimumPurchase;
    @SerializedName("user_cart_data")
    @Expose
    private List<UserCartDatum> userCartData = null;
    public final static Creator<CartModel> CREATOR = new Creator<CartModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CartModel createFromParcel(Parcel in) {
            return new CartModel(in);
        }

        public CartModel[] newArray(int size) {
            return (new CartModel[size]);
        }

    };

    protected CartModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.deliveryCharges = ((String) in.readValue((String.class.getClassLoader())));
        this.minimumPurchase = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.userCartData, (UserCartDatum.class.getClassLoader()));
    }

    public CartModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(String deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public String getMinimumPurchase() {
        return minimumPurchase;
    }

    public void setMinimumPurchase(String minimumPurchase) {
        this.minimumPurchase = minimumPurchase;
    }

    public List<UserCartDatum> getUserCartData() {
        return userCartData;
    }

    public void setUserCartData(List<UserCartDatum> userCartData) {
        this.userCartData = userCartData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(deliveryCharges);
        dest.writeValue(minimumPurchase);
        dest.writeList(userCartData);
    }

    public int describeContents() {
        return 0;
    }

}