package com.shriagrohitech.models.cart;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserCartDatum implements Parcelable {

    @SerializedName("user_cart_id")
    @Expose
    private String userCartId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("variation_id")
    @Expose
    private String variationId;
    @SerializedName("variation_name")
    @Expose
    private String variationName;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("variation_market_price")
    @Expose
    private String variationMarketPrice;
    @SerializedName("variation_selling_price")
    @Expose
    private String variationSellingPrice;
    @SerializedName("variation_quantity")
    @Expose
    private String variationQuantity;
    public final static Creator<UserCartDatum> CREATOR = new Creator<UserCartDatum>() {


        @SuppressWarnings({
                "unchecked"
        })
        public UserCartDatum createFromParcel(Parcel in) {
            return new UserCartDatum(in);
        }

        public UserCartDatum[] newArray(int size) {
            return (new UserCartDatum[size]);
        }

    };

    protected UserCartDatum(Parcel in) {
        this.userCartId = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.productId = ((String) in.readValue((String.class.getClassLoader())));
        this.variationId = ((String) in.readValue((String.class.getClassLoader())));
        this.variationName = ((String) in.readValue((String.class.getClassLoader())));
        this.productName = ((String) in.readValue((String.class.getClassLoader())));
        this.productImage = ((String) in.readValue((String.class.getClassLoader())));
        this.quantity = ((String) in.readValue((String.class.getClassLoader())));
        this.variationMarketPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.variationSellingPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.variationQuantity = ((String) in.readValue((String.class.getClassLoader())));
    }

    public UserCartDatum() {
    }

    public String getUserCartId() {
        return userCartId;
    }

    public void setUserCartId(String userCartId) {
        this.userCartId = userCartId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVariationId() {
        return variationId;
    }

    public void setVariationId(String variationId) {
        this.variationId = variationId;
    }

    public String getVariationName() {
        return variationName;
    }

    public void setVariationName(String variationName) {
        this.variationName = variationName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getVariationMarketPrice() {
        return variationMarketPrice;
    }

    public void setVariationMarketPrice(String variationMarketPrice) {
        this.variationMarketPrice = variationMarketPrice;
    }

    public String getVariationSellingPrice() {
        return variationSellingPrice;
    }

    public void setVariationSellingPrice(String variationSellingPrice) {
        this.variationSellingPrice = variationSellingPrice;
    }

    public String getVariationQuantity() {
        return variationQuantity;
    }

    public void setVariationQuantity(String variationQuantity) {
        this.variationQuantity = variationQuantity;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userCartId);
        dest.writeValue(userId);
        dest.writeValue(productId);
        dest.writeValue(variationId);
        dest.writeValue(variationName);
        dest.writeValue(productName);
        dest.writeValue(productImage);
        dest.writeValue(quantity);
        dest.writeValue(variationMarketPrice);
        dest.writeValue(variationSellingPrice);
        dest.writeValue(variationQuantity);
    }

    public int describeContents() {
        return 0;
    }

}