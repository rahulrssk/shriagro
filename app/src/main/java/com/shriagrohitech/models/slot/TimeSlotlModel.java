package com.shriagrohitech.models.slot;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimeSlotlModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("vendor_time_slot")
    @Expose
    private List<VendorTimeSlot> vendorTimeSlot = null;
    public final static Creator<TimeSlotlModel> CREATOR = new Creator<TimeSlotlModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public TimeSlotlModel createFromParcel(Parcel in) {
            return new TimeSlotlModel(in);
        }

        public TimeSlotlModel[] newArray(int size) {
            return (new TimeSlotlModel[size]);
        }

    };

    protected TimeSlotlModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.vendorTimeSlot, (VendorTimeSlot.class.getClassLoader()));
    }

    public TimeSlotlModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<VendorTimeSlot> getVendorTimeSlot() {
        return vendorTimeSlot;
    }

    public void setVendorTimeSlot(List<VendorTimeSlot> vendorTimeSlot) {
        this.vendorTimeSlot = vendorTimeSlot;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(vendorTimeSlot);
    }

    public int describeContents() {
        return 0;
    }

}