package com.shriagrohitech.models.game_result;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GameResultModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("my_game_history")
    @Expose
    private List<GameResult> gameResult = null;
    public final static Parcelable.Creator<GameResultModel> CREATOR = new Creator<GameResultModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GameResultModel createFromParcel(Parcel in) {
            return new GameResultModel(in);
        }

        public GameResultModel[] newArray(int size) {
            return (new GameResultModel[size]);
        }

    };

    protected GameResultModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.gameResult, (GameResult.class.getClassLoader()));
    }

    public GameResultModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GameResult> getGameResult() {
        return gameResult;
    }

    public void setGameResult(List<GameResult> gameResult) {
        this.gameResult = gameResult;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(gameResult);
    }

    public int describeContents() {
        return 0;
    }

}