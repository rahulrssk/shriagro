package com.shriagrohitech.models.game_result;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GameResult implements Parcelable {

    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("bid_price")
    @Expose
    private String bidPrice;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("current_time")
    @Expose
    private String currentTime;
    @SerializedName("current_date")
    @Expose
    private String currentDate;
    @SerializedName("result_show_time")
    @Expose
    private String resultShowTime;
    @SerializedName("bid_date")
    @Expose
    private String bidDate;
    @SerializedName("user_bid_time")
    @Expose
    private String bidTime;
    public final static Parcelable.Creator<GameResult> CREATOR = new Creator<GameResult>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GameResult createFromParcel(Parcel in) {
            return new GameResult(in);
        }

        public GameResult[] newArray(int size) {
            return (new GameResult[size]);
        }

    };

    protected GameResult(Parcel in) {
        this.userName = ((String) in.readValue((String.class.getClassLoader())));
        this.mobile = ((String) in.readValue((String.class.getClassLoader())));
        this.bidPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.currentTime = ((String) in.readValue((String.class.getClassLoader())));
        this.currentDate = ((String) in.readValue((String.class.getClassLoader())));
        this.resultShowTime = ((String) in.readValue((String.class.getClassLoader())));
        this.bidDate = ((String) in.readValue((String.class.getClassLoader())));
        this.bidTime = ((String) in.readValue((String.class.getClassLoader())));
    }

    public GameResult() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(String bidPrice) {
        this.bidPrice = bidPrice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getResultShowTime() {
        return resultShowTime;
    }

    public void setResultShowTime(String resultShowTime) {
        this.resultShowTime = resultShowTime;
    }

    public String getBidDate() {
        return bidDate;
    }

    public void setBidDate(String bidDate) {
        this.bidDate = bidDate;
    }

    public String getBidTime() {
        return bidTime;
    }

    public void setBidTime(String bidTime) {
        this.bidTime = bidTime;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userName);
        dest.writeValue(mobile);
        dest.writeValue(bidPrice);
        dest.writeValue(image);
        dest.writeValue(currentTime);
        dest.writeValue(currentDate);
        dest.writeValue(resultShowTime);
        dest.writeValue(bidDate);
        dest.writeValue(bidTime);
    }

    public int describeContents() {
        return 0;
    }

}