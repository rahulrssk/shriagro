package com.shriagrohitech.models.offers;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OffersModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("promotion_list")
    @Expose
    private List<PromotionList> promotionList = null;
    public final static Creator<OffersModel> CREATOR = new Creator<OffersModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OffersModel createFromParcel(Parcel in) {
            return new OffersModel(in);
        }

        public OffersModel[] newArray(int size) {
            return (new OffersModel[size]);
        }

    };

    protected OffersModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.promotionList, (PromotionList.class.getClassLoader()));
    }

    public OffersModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PromotionList> getPromotionList() {
        return promotionList;
    }

    public void setPromotionList(List<PromotionList> promotionList) {
        this.promotionList = promotionList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(promotionList);
    }

    public int describeContents() {
        return 0;
    }

}