package com.shriagrohitech.models.offers;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromotionList implements Parcelable {

    @SerializedName("prom_id")
    @Expose
    private String promId;
    @SerializedName("promo_title")
    @Expose
    private String promoTitle;
    @SerializedName("promo_code")
    @Expose
    private String promoCode;
    @SerializedName("promo_percente")
    @Expose
    private String promoPercente;
    @SerializedName("minimum_purchase")
    @Expose
    private String minimumPurchase;
    @SerializedName("promo_dicount_type")
    @Expose
    private String promoDicountType;
    @SerializedName("promo_status")
    @Expose
    private String promoStatus;
    @SerializedName("promo_image")
    @Expose
    private String promoImage;
    public final static Creator<PromotionList> CREATOR = new Creator<PromotionList>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PromotionList createFromParcel(Parcel in) {
            return new PromotionList(in);
        }

        public PromotionList[] newArray(int size) {
            return (new PromotionList[size]);
        }

    };

    protected PromotionList(Parcel in) {
        this.promId = ((String) in.readValue((String.class.getClassLoader())));
        this.promoTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.promoCode = ((String) in.readValue((String.class.getClassLoader())));
        this.promoPercente = ((String) in.readValue((String.class.getClassLoader())));
        this.minimumPurchase = ((String) in.readValue((String.class.getClassLoader())));
        this.promoDicountType = ((String) in.readValue((String.class.getClassLoader())));
        this.promoStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.promoImage = ((String) in.readValue((String.class.getClassLoader())));
    }

    public PromotionList() {
    }

    public String getPromId() {
        return promId;
    }

    public void setPromId(String promId) {
        this.promId = promId;
    }

    public String getPromoTitle() {
        return promoTitle;
    }

    public void setPromoTitle(String promoTitle) {
        this.promoTitle = promoTitle;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoPercente() {
        return promoPercente;
    }

    public void setPromoPercente(String promoPercente) {
        this.promoPercente = promoPercente;
    }

    public String getMinimumPurchase() {
        return minimumPurchase;
    }

    public void setMinimumPurchase(String minimumPurchase) {
        this.minimumPurchase = minimumPurchase;
    }

    public String getPromoDicountType() {
        return promoDicountType;
    }

    public void setPromoDicountType(String promoDicountType) {
        this.promoDicountType = promoDicountType;
    }

    public String getPromoStatus() {
        return promoStatus;
    }

    public void setPromoStatus(String promoStatus) {
        this.promoStatus = promoStatus;
    }

    public String getPromoImage() {
        return promoImage;
    }

    public void setPromoImage(String promoImage) {
        this.promoImage = promoImage;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(promId);
        dest.writeValue(promoTitle);
        dest.writeValue(promoCode);
        dest.writeValue(promoPercente);
        dest.writeValue(minimumPurchase);
        dest.writeValue(promoDicountType);
        dest.writeValue(promoStatus);
        dest.writeValue(promoImage);
    }

    public int describeContents() {
        return 0;
    }

}