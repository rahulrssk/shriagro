package com.shriagrohitech.models.orders;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDatum implements Parcelable {

    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("promo_code")
    @Expose
    private String promoCode;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("delivery_schedule")
    @Expose
    private String deliverySchedule;
    @SerializedName("delivery_charges")
    @Expose
    private String deliveryCharges;
    @SerializedName("address_id")
    @Expose
    private String addressId;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("user_order_status")
    @Expose
    private String userOrderStatus;
    @SerializedName("final_order_status")
    @Expose
    private String finalOrderStatus;
    @SerializedName("order_create_date")
    @Expose
    private String orderCreateDate;
    @SerializedName("product_data")
    @Expose
    private List<ProductDatum> productData = null;
    public final static Creator<OrderDatum> CREATOR = new Creator<OrderDatum>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OrderDatum createFromParcel(Parcel in) {
            return new OrderDatum(in);
        }

        public OrderDatum[] newArray(int size) {
            return (new OrderDatum[size]);
        }

    };

    protected OrderDatum(Parcel in) {
        this.orderId = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.promoCode = ((String) in.readValue((String.class.getClassLoader())));
        this.paymentMethod = ((String) in.readValue((String.class.getClassLoader())));
        this.transactionId = ((String) in.readValue((String.class.getClassLoader())));
        this.totalAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.deliverySchedule = ((String) in.readValue((String.class.getClassLoader())));
        this.deliveryCharges = ((String) in.readValue((String.class.getClassLoader())));
        this.addressId = ((String) in.readValue((String.class.getClassLoader())));
        this.paymentStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.userOrderStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.finalOrderStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.orderCreateDate = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.productData, (ProductDatum.class.getClassLoader()));
    }

    public OrderDatum() {
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDeliverySchedule() {
        return deliverySchedule;
    }

    public void setDeliverySchedule(String deliverySchedule) {
        this.deliverySchedule = deliverySchedule;
    }

    public String getDeliveryCharges() {
        return deliveryCharges;
    }

    public void setDeliveryCharges(String deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getUserOrderStatus() {
        return userOrderStatus;
    }

    public void setUserOrderStatus(String userOrderStatus) {
        this.userOrderStatus = userOrderStatus;
    }

    public String getFinalOrderStatus() {
        return finalOrderStatus;
    }

    public void setFinalOrderStatus(String finalOrderStatus) {
        this.finalOrderStatus = finalOrderStatus;
    }

    public String getOrderCreateDate() {
        return orderCreateDate;
    }

    public void setOrderCreateDate(String orderCreateDate) {
        this.orderCreateDate = orderCreateDate;
    }

    public List<ProductDatum> getProductData() {
        return productData;
    }

    public void setProductData(List<ProductDatum> productData) {
        this.productData = productData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(orderId);
        dest.writeValue(userId);
        dest.writeValue(promoCode);
        dest.writeValue(paymentMethod);
        dest.writeValue(transactionId);
        dest.writeValue(totalAmount);
        dest.writeValue(deliverySchedule);
        dest.writeValue(deliveryCharges);
        dest.writeValue(addressId);
        dest.writeValue(paymentStatus);
        dest.writeValue(userOrderStatus);
        dest.writeValue(finalOrderStatus);
        dest.writeValue(orderCreateDate);
        dest.writeList(productData);
    }

    public int describeContents() {
        return 0;
    }

}