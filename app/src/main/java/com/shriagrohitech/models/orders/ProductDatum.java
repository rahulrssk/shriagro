package com.shriagrohitech.models.orders;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDatum implements Parcelable {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("variation_name")
    @Expose
    private String variationName;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("variation_market_price")
    @Expose
    private String variationMarketPrice;
    @SerializedName("variation_selling_price")
    @Expose
    private String variationSellingPrice;
    @SerializedName("variation_quantity")
    @Expose
    private String variationQuantity;
    public final static Creator<ProductDatum> CREATOR = new Creator<ProductDatum>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ProductDatum createFromParcel(Parcel in) {
            return new ProductDatum(in);
        }

        public ProductDatum[] newArray(int size) {
            return (new ProductDatum[size]);
        }

    };

    protected ProductDatum(Parcel in) {
        this.productId = ((String) in.readValue((String.class.getClassLoader())));
        this.productName = ((String) in.readValue((String.class.getClassLoader())));
        this.variationName = ((String) in.readValue((String.class.getClassLoader())));
        this.quantity = ((String) in.readValue((String.class.getClassLoader())));
        this.variationMarketPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.variationSellingPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.variationQuantity = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ProductDatum() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getVariationName() {
        return variationName;
    }

    public void setVariationName(String variationName) {
        this.variationName = variationName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getVariationMarketPrice() {
        return variationMarketPrice;
    }

    public void setVariationMarketPrice(String variationMarketPrice) {
        this.variationMarketPrice = variationMarketPrice;
    }

    public String getVariationSellingPrice() {
        return variationSellingPrice;
    }

    public void setVariationSellingPrice(String variationSellingPrice) {
        this.variationSellingPrice = variationSellingPrice;
    }

    public String getVariationQuantity() {
        return variationQuantity;
    }

    public void setVariationQuantity(String variationQuantity) {
        this.variationQuantity = variationQuantity;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(productId);
        dest.writeValue(productName);
        dest.writeValue(variationName);
        dest.writeValue(quantity);
        dest.writeValue(variationMarketPrice);
        dest.writeValue(variationSellingPrice);
        dest.writeValue(variationQuantity);
    }

    public int describeContents() {
        return 0;
    }

}