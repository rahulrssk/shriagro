package com.shriagrohitech.models.orders;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyOrdersModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("order_data")
    @Expose
    private List<OrderDatum> orderData = null;
    public final static Creator<MyOrdersModel> CREATOR = new Creator<MyOrdersModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MyOrdersModel createFromParcel(Parcel in) {
            return new MyOrdersModel(in);
        }

        public MyOrdersModel[] newArray(int size) {
            return (new MyOrdersModel[size]);
        }

    };

    protected MyOrdersModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.orderData, (OrderDatum.class.getClassLoader()));
    }

    public MyOrdersModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OrderDatum> getOrderData() {
        return orderData;
    }

    public void setOrderData(List<OrderDatum> orderData) {
        this.orderData = orderData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(orderData);
    }

    public int describeContents() {
        return 0;
    }

}