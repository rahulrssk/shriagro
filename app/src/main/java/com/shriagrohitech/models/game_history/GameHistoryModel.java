package com.shriagrohitech.models.game_history;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GameHistoryModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("my_game_history")
    @Expose
    private List<MyGameHistory> myGameHistory = null;
    public final static Parcelable.Creator<GameHistoryModel> CREATOR = new Creator<GameHistoryModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GameHistoryModel createFromParcel(Parcel in) {
            return new GameHistoryModel(in);
        }

        public GameHistoryModel[] newArray(int size) {
            return (new GameHistoryModel[size]);
        }

    };

    protected GameHistoryModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.myGameHistory, (com.shriagrohitech.models.game_history.MyGameHistory.class.getClassLoader()));
    }

    public GameHistoryModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<MyGameHistory> getMyGameHistory() {
        return myGameHistory;
    }

    public void setMyGameHistory(List<MyGameHistory> myGameHistory) {
        this.myGameHistory = myGameHistory;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(myGameHistory);
    }

    public int describeContents() {
        return 0;
    }

}