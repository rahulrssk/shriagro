package com.shriagrohitech.models.game_history;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyGameHistory implements Parcelable {

    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("game_id")
    @Expose
    private String gameId;
    @SerializedName("bid_price")
    @Expose
    private String bidPrice;
    @SerializedName("bid_date_time")
    @Expose
    private String bidDateTime;
    @SerializedName("game_title")
    @Expose
    private String gameTitle;
    @SerializedName("game_icon")
    @Expose
    private String gameIcon;
    @SerializedName("bid_amount")
    @Expose
    private String bidAmount;
    @SerializedName("total_user_entry")
    @Expose
    private String totalUserEntry;
    @SerializedName("bid_start_time")
    @Expose
    private String bidStartTime;
    @SerializedName("bid_date")
    @Expose
    private String bidDate;
    @SerializedName("bid_end_time")
    @Expose
    private String bidEndTime;
    @SerializedName("result_show_time")
    @Expose
    private String resultShowTime;
    @SerializedName("game_status")
    @Expose
    private String gameStatus;
    public final static Parcelable.Creator<MyGameHistory> CREATOR = new Creator<MyGameHistory>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MyGameHistory createFromParcel(Parcel in) {
            return new MyGameHistory(in);
        }

        public MyGameHistory[] newArray(int size) {
            return (new MyGameHistory[size]);
        }

    };

    protected MyGameHistory(Parcel in) {
        this.bookingId = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.gameId = ((String) in.readValue((String.class.getClassLoader())));
        this.bidPrice = ((String) in.readValue((String.class.getClassLoader())));
        this.bidDateTime = ((String) in.readValue((String.class.getClassLoader())));
        this.gameTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.gameIcon = ((String) in.readValue((String.class.getClassLoader())));
        this.bidAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.totalUserEntry = ((String) in.readValue((String.class.getClassLoader())));
        this.bidStartTime = ((String) in.readValue((String.class.getClassLoader())));
        this.bidDate = ((String) in.readValue((String.class.getClassLoader())));
        this.bidEndTime = ((String) in.readValue((String.class.getClassLoader())));
        this.resultShowTime = ((String) in.readValue((String.class.getClassLoader())));
        this.gameStatus = ((String) in.readValue((String.class.getClassLoader())));
    }

    public MyGameHistory() {
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(String bidPrice) {
        this.bidPrice = bidPrice;
    }

    public String getBidDateTime() {
        return bidDateTime;
    }

    public void setBidDateTime(String bidDateTime) {
        this.bidDateTime = bidDateTime;
    }

    public String getGameTitle() {
        return gameTitle;
    }

    public void setGameTitle(String gameTitle) {
        this.gameTitle = gameTitle;
    }

    public String getGameIcon() {
        return gameIcon;
    }

    public void setGameIcon(String gameIcon) {
        this.gameIcon = gameIcon;
    }

    public String getBidAmount() {
        return bidAmount;
    }

    public void setBidAmount(String bidAmount) {
        this.bidAmount = bidAmount;
    }

    public String getTotalUserEntry() {
        return totalUserEntry;
    }

    public void setTotalUserEntry(String totalUserEntry) {
        this.totalUserEntry = totalUserEntry;
    }

    public String getBidStartTime() {
        return bidStartTime;
    }

    public void setBidStartTime(String bidStartTime) {
        this.bidStartTime = bidStartTime;
    }

    public String getBidDate() {
        return bidDate;
    }

    public void setBidDate(String bidDate) {
        this.bidDate = bidDate;
    }

    public String getBidEndTime() {
        return bidEndTime;
    }

    public void setBidEndTime(String bidEndTime) {
        this.bidEndTime = bidEndTime;
    }

    public String getResultShowTime() {
        return resultShowTime;
    }

    public void setResultShowTime(String resultShowTime) {
        this.resultShowTime = resultShowTime;
    }

    public String getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(bookingId);
        dest.writeValue(userId);
        dest.writeValue(gameId);
        dest.writeValue(bidPrice);
        dest.writeValue(bidDateTime);
        dest.writeValue(gameTitle);
        dest.writeValue(gameIcon);
        dest.writeValue(bidAmount);
        dest.writeValue(totalUserEntry);
        dest.writeValue(bidStartTime);
        dest.writeValue(bidDate);
        dest.writeValue(bidEndTime);
        dest.writeValue(resultShowTime);
        dest.writeValue(gameStatus);
    }

    public int describeContents() {
        return 0;
    }

}