package com.shriagrohitech.models.banner;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Banner implements Parcelable
{

@SerializedName("banner_id")
@Expose
private String bannerId;
@SerializedName("facility_banner")
@Expose
private String facilityBanner;
public final static Parcelable.Creator<Banner> CREATOR = new Creator<Banner>() {


@SuppressWarnings({
"unchecked"
})
public Banner createFromParcel(Parcel in) {
return new Banner(in);
}

public Banner[] newArray(int size) {
return (new Banner[size]);
}

}
;

protected Banner(Parcel in) {
this.bannerId = ((String) in.readValue((String.class.getClassLoader())));
this.facilityBanner = ((String) in.readValue((String.class.getClassLoader())));
}

public Banner() {
}

public String getBannerId() {
return bannerId;
}

public void setBannerId(String bannerId) {
this.bannerId = bannerId;
}

public String getFacilityBanner() {
return facilityBanner;
}

public void setFacilityBanner(String facilityBanner) {
this.facilityBanner = facilityBanner;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(bannerId);
dest.writeValue(facilityBanner);
}

public int describeContents() {
return 0;
}

}