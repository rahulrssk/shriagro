package com.shriagrohitech.models.banner;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeBannerModel implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("banner")
    @Expose
    private List<Banner> banner = null;
    public final static Parcelable.Creator<HomeBannerModel> CREATOR = new Creator<HomeBannerModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HomeBannerModel createFromParcel(Parcel in) {
            return new HomeBannerModel(in);
        }

        public HomeBannerModel[] newArray(int size) {
            return (new HomeBannerModel[size]);
        }

    }
            ;

    protected HomeBannerModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.banner, (com.shriagrohitech.models.banner.Banner.class.getClassLoader()));
    }

    public HomeBannerModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Banner> getBanner() {
        return banner;
    }

    public void setBanner(List<Banner> banner) {
        this.banner = banner;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(banner);
    }

    public int describeContents() {
        return 0;
    }

}