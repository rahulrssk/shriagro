package com.shriagrohitech.models.game_home;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GameData implements Parcelable {

    @SerializedName("game_id")
    @Expose
    private String gameId;
    @SerializedName("game_title")
    @Expose
    private String gameTitle;
    @SerializedName("game_icon")
    @Expose
    private String gameIcon;
    @SerializedName("bid_amount")
    @Expose
    private String bidAmount;
    @SerializedName("total_user_entry")
    @Expose
    private String totalUserEntry;
    @SerializedName("current_time")
    @Expose
    private String currentTime;
    @SerializedName("current_date")
    @Expose
    private String currentDate;
    @SerializedName("bid_start_time")
    @Expose
    private String bidStartTime;
    @SerializedName("bid_end_time")
    @Expose
    private String bidEndTime;
    @SerializedName("bid_date")
    @Expose
    private String bidDate;
    @SerializedName("result_show_time")
    @Expose
    private String resultShowTime;
    @SerializedName("total_bid")
    @Expose
    private int totalBid;
    @SerializedName("user_bid")
    @Expose
    private int userBid;
    public final static Parcelable.Creator<GameData> CREATOR = new Creator<GameData>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GameData createFromParcel(Parcel in) {
            return new GameData(in);
        }

        public GameData[] newArray(int size) {
            return (new GameData[size]);
        }

    };

    protected GameData(Parcel in) {
        this.gameId = ((String) in.readValue((String.class.getClassLoader())));
        this.gameTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.gameIcon = ((String) in.readValue((String.class.getClassLoader())));
        this.bidAmount = ((String) in.readValue((String.class.getClassLoader())));
        this.totalUserEntry = ((String) in.readValue((String.class.getClassLoader())));
        this.currentTime = ((String) in.readValue((String.class.getClassLoader())));
        this.currentDate = ((String) in.readValue((String.class.getClassLoader())));
        this.bidStartTime = ((String) in.readValue((String.class.getClassLoader())));
        this.bidEndTime = ((String) in.readValue((String.class.getClassLoader())));
        this.bidDate = ((String) in.readValue((String.class.getClassLoader())));
        this.resultShowTime = ((String) in.readValue((String.class.getClassLoader())));
        this.totalBid = ((int) in.readValue((int.class.getClassLoader())));
        this.userBid = ((int) in.readValue((int.class.getClassLoader())));
    }

    public GameData() {
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getGameTitle() {
        return gameTitle;
    }

    public void setGameTitle(String gameTitle) {
        this.gameTitle = gameTitle;
    }

    public String getGameIcon() {
        return gameIcon;
    }

    public void setGameIcon(String gameIcon) {
        this.gameIcon = gameIcon;
    }

    public String getBidAmount() {
        return bidAmount;
    }

    public void setBidAmount(String bidAmount) {
        this.bidAmount = bidAmount;
    }

    public String getTotalUserEntry() {
        return totalUserEntry;
    }

    public void setTotalUserEntry(String totalUserEntry) {
        this.totalUserEntry = totalUserEntry;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getBidStartTime() {
        return bidStartTime;
    }

    public void setBidStartTime(String bidStartTime) {
        this.bidStartTime = bidStartTime;
    }

    public String getBidEndTime() {
        return bidEndTime;
    }

    public void setBidEndTime(String bidEndTime) {
        this.bidEndTime = bidEndTime;
    }

    public String getBidDate() {
        return bidDate;
    }

    public void setBidDate(String bidDate) {
        this.bidDate = bidDate;
    }

    public String getResultShowTime() {
        return resultShowTime;
    }

    public void setResultShowTime(String resultShowTime) {
        this.resultShowTime = resultShowTime;
    }

    public int getTotalBid() {
        return totalBid;
    }

    public void setTotalBid(int totalBid) {
        this.totalBid = totalBid;
    }

    public int getUserBid() {
        return userBid;
    }

    public void setUserBid(int userBid) {
        this.userBid = userBid;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(gameId);
        dest.writeValue(gameTitle);
        dest.writeValue(gameIcon);
        dest.writeValue(bidAmount);
        dest.writeValue(totalUserEntry);
        dest.writeValue(currentTime);
        dest.writeValue(currentDate);
        dest.writeValue(bidStartTime);
        dest.writeValue(bidEndTime);
        dest.writeValue(bidDate);
        dest.writeValue(resultShowTime);
        dest.writeValue(totalBid);
        dest.writeValue(userBid);

/*"game_details": {
        "game_id": "3",
        "game_title": "xyz",
        "game_icon": "http://vintiwebsolution.com/bc/assets/game/",
        "bid_amount": "20",
        "total_user_entry": "50",
        "current_time": "15:27:58",
        "current_date": "2020-10-22",
        "bid_start_time": "15:00",
        "bid_end_time": "15:30",
        "bid_date": "2020-10-22",
        "result_show_time": "15:45",
        "total_bid": 1
    }*/
    }

    public int describeContents() {
        return 0;
    }

}