package com.shriagrohitech.models.game_home;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GameDetailModel implements Parcelable
{

@SerializedName("status")
@Expose
private Integer status;
@SerializedName("message")
@Expose
private String message;
@SerializedName("game_details")
@Expose
private GameData gameDetails;
public final static Parcelable.Creator<GameDetailModel> CREATOR = new Creator<GameDetailModel>() {


@SuppressWarnings({
"unchecked"
})
public GameDetailModel createFromParcel(Parcel in) {
return new GameDetailModel(in);
}

public GameDetailModel[] newArray(int size) {
return (new GameDetailModel[size]);
}

}
;

protected GameDetailModel(Parcel in) {
this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
this.message = ((String) in.readValue((String.class.getClassLoader())));
this.gameDetails = ((GameData) in.readValue((GameData.class.getClassLoader())));
}

public GameDetailModel() {
}

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public GameData getGameDetails() {
return gameDetails;
}

public void setGameDetails(GameData gameDetails) {
this.gameDetails = gameDetails;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(status);
dest.writeValue(message);
dest.writeValue(gameDetails);
}

public int describeContents() {
return 0;
}

}