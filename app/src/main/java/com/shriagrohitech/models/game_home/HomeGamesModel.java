package com.shriagrohitech.models.game_home;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeGamesModel implements Parcelable
{

@SerializedName("status")
@Expose
private Integer status;
@SerializedName("message")
@Expose
private String message;
@SerializedName("sport_category")
@Expose
private List<GameData> sportCategory = null;
public final static Parcelable.Creator<HomeGamesModel> CREATOR = new Creator<HomeGamesModel>() {


@SuppressWarnings({
"unchecked"
})
public HomeGamesModel createFromParcel(Parcel in) {
return new HomeGamesModel(in);
}

public HomeGamesModel[] newArray(int size) {
return (new HomeGamesModel[size]);
}

}
;

protected HomeGamesModel(Parcel in) {
this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
this.message = ((String) in.readValue((String.class.getClassLoader())));
in.readList(this.sportCategory, (GameData.class.getClassLoader()));
}

public HomeGamesModel() {
}

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public List<GameData> getSportCategory() {
return sportCategory;
}

public void setSportCategory(List<GameData> sportCategory) {
this.sportCategory = sportCategory;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(status);
dest.writeValue(message);
dest.writeList(sportCategory);
}

public int describeContents() {
return 0;
}

}