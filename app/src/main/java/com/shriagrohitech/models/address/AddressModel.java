package com.shriagrohitech.models.address;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddressModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_address")
    @Expose
    private List<UserAddress> userAddress = null;
    public final static Creator<AddressModel> CREATOR = new Creator<AddressModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AddressModel createFromParcel(Parcel in) {
            return new AddressModel(in);
        }

        public AddressModel[] newArray(int size) {
            return (new AddressModel[size]);
        }

    };

    protected AddressModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.userAddress, (UserAddress.class.getClassLoader()));
    }

    public AddressModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserAddress> getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(List<UserAddress> userAddress) {
        this.userAddress = userAddress;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(userAddress);
    }

    public int describeContents() {
        return 0;
    }

}