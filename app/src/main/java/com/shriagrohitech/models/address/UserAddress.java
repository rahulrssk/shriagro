package com.shriagrohitech.models.address;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAddress implements Parcelable {

    @SerializedName("address_id")
    @Expose
    private String addressId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("add")
    @Expose
    private String add;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("add_type")
    @Expose
    private String addType;
    @SerializedName("add_contact")
    @Expose
    private String addContact;
    @SerializedName("add_alternet_contact")
    @Expose
    private String addAlternetContact;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    public final static Creator<UserAddress> CREATOR = new Creator<UserAddress>() {


        @SuppressWarnings({
                "unchecked"
        })
        public UserAddress createFromParcel(Parcel in) {
            return new UserAddress(in);
        }

        public UserAddress[] newArray(int size) {
            return (new UserAddress[size]);
        }

    };

    protected UserAddress(Parcel in) {
        this.addressId = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((String) in.readValue((String.class.getClassLoader())));
        this.add = ((String) in.readValue((String.class.getClassLoader())));
        this.city = ((String) in.readValue((String.class.getClassLoader())));
        this.state = ((String) in.readValue((String.class.getClassLoader())));
        this.zipcode = ((String) in.readValue((String.class.getClassLoader())));
        this.addType = ((String) in.readValue((String.class.getClassLoader())));
        this.addContact = ((String) in.readValue((String.class.getClassLoader())));
        this.addAlternetContact = ((String) in.readValue((String.class.getClassLoader())));
        this.landmark = ((String) in.readValue((String.class.getClassLoader())));
    }

    public UserAddress() {
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getAddType() {
        return addType;
    }

    public void setAddType(String addType) {
        this.addType = addType;
    }

    public String getAddContact() {
        return addContact;
    }

    public void setAddContact(String addContact) {
        this.addContact = addContact;
    }

    public String getAddAlternetContact() {
        return addAlternetContact;
    }

    public void setAddAlternetContact(String addAlternetContact) {
        this.addAlternetContact = addAlternetContact;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(addressId);
        dest.writeValue(userId);
        dest.writeValue(add);
        dest.writeValue(city);
        dest.writeValue(state);
        dest.writeValue(zipcode);
        dest.writeValue(addType);
        dest.writeValue(addContact);
        dest.writeValue(addAlternetContact);
        dest.writeValue(landmark);
    }

    public int describeContents() {
        return 0;
    }

}