package com.shriagrohitech.models.wallet;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletDataModel implements Parcelable
{

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("total_wallet_Amount")
    @Expose
    private double totalWalletAmount;
    @SerializedName("wallet_history")
    @Expose
    private List<WalletHistory> walletHistory = null;
    public final static Parcelable.Creator<WalletDataModel> CREATOR = new Creator<WalletDataModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public WalletDataModel createFromParcel(Parcel in) {
            return new WalletDataModel(in);
        }

        public WalletDataModel[] newArray(int size) {
            return (new WalletDataModel[size]);
        }

    }
            ;

    protected WalletDataModel(Parcel in) {
        this.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.totalWalletAmount = ((double) in.readValue((double.class.getClassLoader())));
        in.readList(this.walletHistory, (com.shriagrohitech.models.wallet.WalletHistory.class.getClassLoader()));
    }

    public WalletDataModel() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getTotalWalletAmount() {
        return totalWalletAmount;
    }

    public void setTotalWalletAmount(double totalWalletAmount) {
        this.totalWalletAmount = totalWalletAmount;
    }

    public List<WalletHistory> getWalletHistory() {
        return walletHistory;
    }

    public void setWalletHistory(List<WalletHistory> walletHistory) {
        this.walletHistory = walletHistory;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(totalWalletAmount);
        dest.writeList(walletHistory);
    }

    public int describeContents() {
        return 0;
    }

}