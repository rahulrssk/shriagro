package com.shriagrohitech.models.wallet;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletHistory implements Parcelable {

    @SerializedName("wallet_id")
    @Expose
    private String walletId;
    @SerializedName("amount_credit")
    @Expose
    private String amountCredit;
    @SerializedName("amount_debit")
    @Expose
    private String amountDebit;
    @SerializedName("credit_type")
    @Expose
    private String creditType;
    @SerializedName("transaction_datetime")
    @Expose
    private String transactionDatetime;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("transaction_type")
    @Expose
    private String transactionType;
    public final static Parcelable.Creator<WalletHistory> CREATOR = new Creator<WalletHistory>() {


        @SuppressWarnings({
                "unchecked"
        })
        public WalletHistory createFromParcel(Parcel in) {
            return new WalletHistory(in);
        }

        public WalletHistory[] newArray(int size) {
            return (new WalletHistory[size]);
        }

    };

    protected WalletHistory(Parcel in) {
        this.walletId = ((String) in.readValue((String.class.getClassLoader())));
        this.amountCredit = ((String) in.readValue((String.class.getClassLoader())));
        this.amountDebit = ((String) in.readValue((String.class.getClassLoader())));
        this.creditType = ((String) in.readValue((String.class.getClassLoader())));
        this.transactionDatetime = ((String) in.readValue((String.class.getClassLoader())));
        this.paymentStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.transactionType = ((String) in.readValue((String.class.getClassLoader())));
    }

    public WalletHistory() {
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getAmountCredit() {
        return amountCredit;
    }

    public void setAmountCredit(String amountCredit) {
        this.amountCredit = amountCredit;
    }

    public String getAmountDebit() {
        return amountDebit;
    }

    public void setAmountDebit(String amountDebit) {
        this.amountDebit = amountDebit;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getTransactionDatetime() {
        return transactionDatetime;
    }

    public void setTransactionDatetime(String transactionDatetime) {
        this.transactionDatetime = transactionDatetime;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(walletId);
        dest.writeValue(amountCredit);
        dest.writeValue(amountDebit);
        dest.writeValue(creditType);
        dest.writeValue(transactionDatetime);
        dest.writeValue(paymentStatus);
        dest.writeValue(transactionType);
    }

    public int describeContents() {
        return 0;
    }

}