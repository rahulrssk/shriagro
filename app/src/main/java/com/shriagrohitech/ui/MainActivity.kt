package com.shriagrohitech.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.chatair_android.utils.showLongToast
import com.shriagrohitech.R
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.ui.fragment.*
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : BaseActivity() {

    private var doubleBackToExitPressedOnce = false
    private var fTag = "Home"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        val userObject =
            JSONObject(AppPreference.getStringPreference(mContext!!, Constant.USER_DATA))
        val name = userObject.getString("name")
        val village = userObject.getString("gaon")

        if (name.isNotEmpty() && village.isNotEmpty()){
            AppPreference.setBooleanPreference(
                mContext!!,
                Constant.IS_COMPLETE_ADDRESS,
                true
            )

            initHomeMenu()
        }else{
            AppPreference.setBooleanPreference(
                mContext!!,
                Constant.IS_COMPLETE_ADDRESS,
                false
            )
            initAccountMenu()
        }

        menuHome.setOnClickListener {
            if (AppPreference.getBooleanPreference(mContext!!, Constant.IS_COMPLETE_ADDRESS)) {
                initHomeMenu()
            } else {
                mContext!!.showLongToast("First Complete Your Profile...")
            }
        }
        menuMyMatches.setOnClickListener {
            if (AppPreference.getBooleanPreference(mContext!!, Constant.IS_COMPLETE_ADDRESS)) {
                initMyMatchesMenu()
            } else {
                mContext!!.showLongToast("First Complete Your Profile...")
            }
        }
        menuAccount.setOnClickListener {
            //if (AppPreference.getBooleanPreference(mContext!!, Constant.IS_COMPLETE_ADDRESS)) {
                initAccountMenu()
            /*} else {
                mContext!!.showLongToast("First Complete Your Profile...")
            }*/
        }
        menuOffers.setOnClickListener {
            if (AppPreference.getBooleanPreference(mContext!!, Constant.IS_COMPLETE_ADDRESS)) {
                initOffersMenu()
            } else {
                mContext!!.showLongToast("First Complete Your Profile...")
            }
        }
        menuMore.setOnClickListener {
            if (AppPreference.getBooleanPreference(mContext!!, Constant.IS_COMPLETE_ADDRESS)) {
                initMoreMenu()
            } else {
                mContext!!.showLongToast("First Complete Your Profile...")
            }
        }
        /*menuWallet.setOnClickListener { startActivity(Intent(mContext!!,
            WalletActivity::class.java)) }*/

        ivCart.setOnClickListener {
            if (tvCartCount.text.toString()!="0"){
                startActivity(Intent(mContext!!, CartActivity::class.java))
            }else{
                showToast(mContext!!, "Cart is Empty!!!")
            }
        }

    }

    private fun initHomeMenu() {
        
        ivHome.setImageResource(R.drawable.icf_home_active)
        tvHome.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorPrimary))

        ivMyMatches.setImageResource(R.drawable.icf_file)
        tvMyMatches.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivAccount.setImageResource(R.drawable.icf_user)
        tvAccount.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivOffers.setImageResource(R.drawable.icf_offers)
        tvOffers.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivMore.setImageResource(R.drawable.icf_more)
        tvMore.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        replaceFragment(HomeFragment(), "Home")
    }

    private fun initMyMatchesMenu() {
        ivHome.setImageResource(R.drawable.icf_home)
        tvHome.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivMyMatches.setImageResource(R.drawable.icf_file_active)
        tvMyMatches.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorPrimary))

        ivAccount.setImageResource(R.drawable.icf_user)
        tvAccount.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivOffers.setImageResource(R.drawable.icf_offers)
        tvOffers.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivMore.setImageResource(R.drawable.icf_more)
        tvMore.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        replaceFragment(MyOrdersFragment(), "MyOrders")
    }

    private fun initAccountMenu() {
        ivHome.setImageResource(R.drawable.icf_home)
        tvHome.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivMyMatches.setImageResource(R.drawable.icf_file)
        tvMyMatches.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivAccount.setImageResource(R.drawable.icf_user_active)
        tvAccount.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorPrimary))

        ivOffers.setImageResource(R.drawable.icf_offers)
        tvOffers.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivMore.setImageResource(R.drawable.icf_more)
        tvMore.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        replaceFragment(AccountFragment(), "MyProfile")
    }

    private fun initOffersMenu() {
        ivHome.setImageResource(R.drawable.icf_home)
        tvHome.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivMyMatches.setImageResource(R.drawable.icf_file)
        tvMyMatches.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivAccount.setImageResource(R.drawable.icf_user)
        tvAccount.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivOffers.setImageResource(R.drawable.icf_offers_active)
        tvOffers.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorPrimary))

        ivMore.setImageResource(R.drawable.icf_more)
        tvMore.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        replaceFragment(OffersFragment(), "Offers")
    }

    private fun initMoreMenu() {
        ivHome.setImageResource(R.drawable.icf_home)
        tvHome.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivMyMatches.setImageResource(R.drawable.icf_file)
        tvMyMatches.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivAccount.setImageResource(R.drawable.icf_user)
        tvAccount.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivOffers.setImageResource(R.drawable.icf_offers)
        tvOffers.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorGray))

        ivMore.setImageResource(R.drawable.icf_more_active)
        tvMore.setTextColor(ContextCompat.getColor(this@MainActivity, R.color.colorPrimary))

        replaceFragment(MoreFragment(), "More")
    }

    private fun replaceFragment(
        fragment: Fragment,
        tag: String
    ) {
        fTag = tag
        val fragmentManager = supportFragmentManager
        fragmentManager.beginTransaction()
            .replace(R.id.frame, fragment, tag)
            .commit()
    }



    override fun onBackPressed() {
        if (AppPreference.getBooleanPreference(mContext!!, Constant.IS_COMPLETE_ADDRESS)) {
            if (fTag == "Home") {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed()
                    return
                }

                this.doubleBackToExitPressedOnce = true
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

                Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
            }else{
                initHomeMenu()
            }
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
                return
            }

            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

            Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
        }
    }
}