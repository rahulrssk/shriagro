package com.shriagrohitech.ui

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.chatair_android.utils.showShortToast
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import com.shriagrohitech.R
import com.shriagrohitech.adapter.TimeSlotAdapter
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.models.address.UserAddress
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import com.shriagrohitech.models.slot.TimeSlotlModel
import com.shriagrohitech.models.slot.VendorTimeSlot
import kotlinx.android.synthetic.main.activity_checkout.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CheckoutActivity : BaseActivity(), View.OnClickListener, PaymentResultListener {
    private var totalCartAmount = "0"
    private var totalAmount = 0.0
    private var walletAmount = 0
    private var timeSlot: ArrayList<VendorTimeSlot> = ArrayList()
    private var timeSlotAdapter: TimeSlotAdapter? = null
    private var isPayment = false
    private var deliveryDay = "Today"
    private var selectedTimeSlot: VendorTimeSlot? = null
    private var selectedAddress: UserAddress? = null
    private var paymentMode = "COD"
    private var couponCode = ""

    private var discountAmount = 0
    private var totalPrice: Double = 0.0
    private var grossPrice: Double = 0.0

    private var deliveryCharge = 0
    private var minimumPurchase = 0
    private var applicableDC = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)

        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        ivBack.setOnClickListener { onBackPressed() }

        totalCartAmount = intent.extras!!.getString("Amount")!!
        totalAmount = totalCartAmount.toDouble()
        totalPrice = totalAmount
        grossPrice = totalPrice

        /*try {
            deliveryCharge = intent!!.extras!!.getString("deliveryCharge")!!.toInt()
            minimumPurchase = intent!!.extras!!.getString("minimumPurchase")!!.toInt()
            tvNoteMessage.text = "Delivery charge applicable on order bellow ₹$minimumPurchase"
        } catch (e: Exception) {
            e.printStackTrace()
        }*/

        /*if (totalPrice < minimumPurchase) {
            grossPrice += deliveryCharge
            tvDeliveryCharge.text = "Delivery Charge ₹$deliveryCharge.00 Applicable."
            applicableDC = deliveryCharge.toString()
        } else {
            tvDeliveryCharge.text = "Free shipping"
            applicableDC = "0"
        }*/

        val strPrc = "%.2f".format(grossPrice)
        tvTotalAmount.text = "Toatl : ₹$strPrc"

        btnCheckout.setOnClickListener {
            if (isPayment) {
                when (paymentMode) {
                    "COD" -> {
                        proceedToCheckOut("0", "Pending")
                    }
                    "ONLINE" -> {
                        requestPayment()
                    }
                    else -> {
                        mContext!!.showShortToast("No Payment Mode Selected")
                    }
                }
            } else {
                deliveryDay = if (rgDay.checkedRadioButtonId == R.id.rbTomorrow) {
                    "Tomorrow"
                } else {
                    "Today"
                }
                selectedTimeSlot = getSelectedTimeSlot()
                if (selectedAddress == null) {
                    mContext!!.showShortToast("No Address selected...")
                } else if (selectedTimeSlot == null) {
                    mContext!!.showShortToast("No Time Slot selected...")
                } else {
                    when (paymentMode) {
                        "COD" -> {
                            proceedToCheckOut("0", "Pending")
                        }
                        "ONLINE" -> {
                            requestPayment()
                        }
                        else -> {
                            mContext!!.showShortToast("No Payment Mode Selected")
                        }
                    }
                }
            }
        }

        tvOnlinePayment.setBackgroundColor(resources.getColor(R.color.app_bg))
        tvWalletPayment.setBackgroundColor(resources.getColor(R.color.app_bg))
        tvCod.background = resources.getDrawable(R.drawable.bg_selected_mode)
        paymentMode = "COD"

        /*tvCod.setOnClickListener {
            tvOnlinePayment.setBackgroundColor(resources.getColor(R.color.app_bg))
            tvWalletPayment.setBackgroundColor(resources.getColor(R.color.app_bg))
            tvCod.background = resources.getDrawable(R.drawable.bg_selected_mode)
            paymentMode = "COD"
        }
        tvOnlinePayment.setOnClickListener {
            tvCod.setBackgroundColor(resources.getColor(R.color.app_bg))
            tvWalletPayment.setBackgroundColor(resources.getColor(R.color.app_bg))
            tvOnlinePayment.background = resources.getDrawable(R.drawable.bg_selected_mode)
            paymentMode = "ONLINE"
        }
        tvWalletPayment.setOnClickListener {
            if (walletAmount>=totalAmount) {
                tvCod.setBackgroundColor(resources.getColor(R.color.app_bg))
                tvWalletPayment.background = resources.getDrawable(R.drawable.bg_selected_mode)
                tvOnlinePayment.setBackgroundColor(resources.getColor(R.color.app_bg))
                paymentMode = "WALLET"
            }else{
                mContext!!.showLongToast("You don't have enough balance in cart for this order...")
            }
        }*/
        tvSelectAddress.setOnClickListener {
            startActivityForResult(
                Intent(mContext, AddressListActivity::class.java), 312
            )
        }

        btnApplyCoupon.setOnClickListener {
            val promoCode = etPromoCode.text.toString().trim()
            if (promoCode.isEmpty()) {
                showToast(mContext!!, "First Enter a Promo Code...")
            } else {
                if (cd!!.isNetworkAvailable) {
                    val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)

                    RetrofitService.getServerResponse(Dialog(mContext!!),
                        retrofitApiClient!!.checkCoupon(userId, promoCode), object : WebResponse {

                            override fun onResponseFailed(error: String?) {
                                mContext!!.showShortToast("Server Error...")
                            }

                            /*{
                              "status": 200,
                              "message": "Valid Promotion Code",
                              "promotion_data": [
                                {
                                  "prom_id": "1",
                                  "prom_image": "download_(9).jpg",
                                  "promo_title": "HOLI OFFER",
                                  "promo_code": "HOLI2020",
                                  "promo_percente": "10",
                                  "start_date": "",
                                  "end_date": "",
                                  "minimum_purchase": "1000",
                                  "promo_dicount_type": "Percent",
                                  "promo_status": "Activate"
                                }
                              ]
                            }*/
                            override fun onResponseSuccess(result: Response<*>?) {
                                val responseBody = result!!.body() as ResponseBody?
                                val responseData = JSONObject(responseBody!!.string())
                                val status = responseData.getInt("status")
                                val message = responseData.getString("message")
                                tvCouponMessage.visibility = View.VISIBLE
                                if (status==200){
                                    couponCode = ""
                                    val promotionData = responseData
                                        .getJSONArray("promotion_data")[0] as JSONObject
                                    val minimumPurchaseCoupon: Int = promotionData
                                        .getString("minimum_purchase").toInt()
                                    if (totalPrice<minimumPurchaseCoupon){
                                        tvCouponMessage.text = "This Promo Code will apply on" +
                                                " purchase above ₹$minimumPurchaseCoupon"
                                    }else{
                                        btnApplyCoupon.isEnabled = false
                                        etPromoCode.isFocusable = false
                                        etPromoCode.isFocusableInTouchMode = false
                                        couponCode = promotionData.getString("promo_code")
                                        var dAmount = 0
                                        val discountType = promotionData
                                            .getString("promo_dicount_type")
                                        if (discountType=="Percent"){
                                            val dPercent = promotionData
                                                .getString("promo_percente").toInt()
                                            dAmount = (dPercent*totalPrice.toInt())/100
                                        }else if(discountType=="Amount"){
                                            val da = promotionData
                                                .getString("promo_percente").toInt()
                                            dAmount = da
                                        }

                                        grossPrice = totalPrice-dAmount
                                        /*if (grossPrice < minimumPurchase) {
                                            grossPrice += deliveryCharge
                                            tvDeliveryCharge.text = "Delivery Charge ₹$deliveryCharge.00 Applicable."
                                            applicableDC = deliveryCharge.toString()
                                        } else {
                                            tvDeliveryCharge.text = "Free shipping"
                                            applicableDC = "0"
                                        }*/

                                        val strPrc = "%.2f".format(grossPrice)
                                        tvTotalAmount.text = "Toatl : ₹$strPrc"
                                        tvCouponMessage.text = "Promo Code Applied"
                                    }
                                }else if (status==400){
                                    tvCouponMessage.text = "Invalid Promo Code"
                                }else{
                                    tvCouponMessage.text = "Invalid Promo Code"
                                }

                            }
                        })
                }
            }
        }

        fetchTimeSlot()
    }

    private fun getSelectedTimeSlot(): VendorTimeSlot? {
        var slot: VendorTimeSlot? = null
        timeSlot.forEach {
            if (it.isSelected) {
                slot = it
            }
        }
        return slot
    }

    private fun fetchTimeSlot() {
        if (cd!!.isNetworkAvailable) {
            RetrofitService.timeSlotResponse(
                Dialog(mContext!!), retrofitApiClient!!.timeSlot(),
                object : WebResponse {
                    override fun onResponseFailed(error: String?) {

                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as TimeSlotlModel
                        if (response.status == 200) {
                            if (response.vendorTimeSlot.size > 0) {
                                timeSlot = response.vendorTimeSlot as ArrayList<VendorTimeSlot>
                                timeSlot[0].isSelected = true
                                rvSlot.layoutManager = LinearLayoutManager(mContext)
                                timeSlotAdapter =
                                    TimeSlotAdapter(timeSlot, mContext!!, this@CheckoutActivity)
                                rvSlot.adapter = timeSlotAdapter
                                timeSlotAdapter!!.notifyDataSetChanged()
                            }
                        }
                    }
                })
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.rbChecked -> {
                val tag = p0.tag as Int
                for (i in timeSlot.indices) {
                    timeSlot[i].isSelected = i == tag
                }
                timeSlotAdapter!!.notifyDataSetChanged()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 312) {
            selectedAddress = data?.getParcelableExtra("addressId")
            if (selectedAddress != null) {
                tvAddressDetail.setText(
                    selectedAddress!!.add +
                            "," + selectedAddress!!.city + "," +
                            selectedAddress!!.state + "-" + selectedAddress!!.zipcode
                )
                tvContact.setText("Contact : " + selectedAddress!!.addContact)
                tvSelectAddress.text = "Change Address"
                rlAddressField.visibility = View.VISIBLE
            }
        }
    }

    /******************************************************************/
    /*****************RAZOR PAY Payment Flow*/
    /******************************************************************/

    private fun requestPayment() {
        val activity: Activity = this
        val co = Checkout()

        var amt = 0

        try {
            amt = (totalAmount * 100).toInt()
        } catch (e: Exception) {
        }

        try {
            val options = JSONObject()
            options.put("name", "Samon")
            options.put("description", "Pay Now")
            //You can omit the image option to fetch the image from dashboard
            //options.put("image","https://urbanclean.org/mojo/images/img/LOGOo.png")
            options.put("currency", "INR")
            options.put("amount", "$amt")
            //options.put("amount","100")


            val userData =
                JSONObject(AppPreference.getStringPreference(mContext, Constant.USER_DATA))
            val uMobile = userData.getString("mobile")
            val uEmail = userData.getString("email")
            val prefill = JSONObject()
            prefill.put("email", uEmail)
            prefill.put("contact", uMobile)

            options.put("prefill", prefill)
            co.open(activity, options)
        } catch (e: Exception) {
            Toast.makeText(activity, "Error in payment: " + e.message, Toast.LENGTH_LONG).show()
            e.printStackTrace()
        }
    }

    override fun onPaymentError(errorCode: Int, response: String?) {
        mContext!!.showShortToast(response!!)
    }

    override fun onPaymentSuccess(razorpayPaymentId: String) {
        proceedToCheckOut(razorpayPaymentId, "Credit")
    }

    private fun proceedToCheckOut(trId: String, paymentStatus: String) {
        if (cd!!.isNetworkAvailable) {
            val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
            val strPrc = "%.2f".format(grossPrice)
            RetrofitService.getServerResponse(
                Dialog(mContext!!), retrofitApiClient!!.userOrder(
                    userId, couponCode, paymentMode, trId, strPrc, selectedTimeSlot!!.timeSlot,
                    applicableDC, selectedAddress!!.addressId, getCurrentDate(), paymentStatus
                ),
                object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as ResponseBody
                        val responseObject = JSONObject(response.string())
                        val status = responseObject.getInt("status")
                        val msg = responseObject.getString("message")
                        if (status == 200) {
                            startActivity(Intent(mContext!!, OrderCompleteActivity::class.java))
                        }
                        mContext!!.showShortToast(msg)
                    }
                })
        }
    }

    private fun getCurrentDate(): String? {
        // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US)

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar: Calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        return formatter.format(calendar.time)
    }

    /*user_id=1
    promo_code=HOLI2020
    payment_method=COD
    transaction_id=0
    total_amount=1067.00
    delivery_schedule=9%3A00%20AM-8%3A00%20PM
    delivery_charges=0
    address_id=5
    order_create_date=13-02-2021%2010%3A17%20AM
    payment_status=Pending

    user_id=1
    promo_code=
    payment_method=COD
    transaction_id=0
    total_amount=175.00
    delivery_schedule=9%3A00%20AM-8%3A00%20PM
    delivery_charges=40
    address_id=5
    order_create_date=13-02-2021%2010%3A19%20AM
    payment_status=Pending*/

}