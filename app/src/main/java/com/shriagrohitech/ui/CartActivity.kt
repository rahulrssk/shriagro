package com.shriagrohitech.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.chatair_android.utils.showShortToast
import com.infobitetechnology.samon.interfaces.CartListener
import com.shriagrohitech.R
import com.shriagrohitech.adapter.ProductCartAdapter
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.models.cart.CartModel
import com.shriagrohitech.models.cart.UserCartDatum
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_cart.ivBack
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

class CartActivity : BaseActivity(), CartListener {

    private var cartData: ArrayList<UserCartDatum> = ArrayList()
    private var cartAdapter: ProductCartAdapter? = null
    private var totalCartAmount = "0.00"
    private lateinit var tvCartCount : TextView
    private var deliveryCharge = ""
    private var minimumPurchase = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)

        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        ivBack.setOnClickListener { onBackPressed() }
        tvCartCount = findViewById(R.id.tvCartCount)
        btnCheckout.setOnClickListener { startActivity(
            Intent(mContext!!,
            CheckoutActivity::class.java).putExtra("Amount", totalCartAmount)
                .putExtra("deliveryCharge", deliveryCharge)
                .putExtra("minimumPurchase", minimumPurchase)) }
        cartData()
    }

    private fun cartData() {
        if (cd!!.isNetworkAvailable) {
            val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
            RetrofitService.cartDataResponse(
                Dialog(mContext!!),
                retrofitApiClient!!.cartData(userId), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as CartModel
                        if (response.status == 200) {

                            if (response.userCartData.size > 0) {
                                deliveryCharge = response.deliveryCharges
                                minimumPurchase = response.minimumPurchase
                                cartData = response.userCartData as ArrayList<UserCartDatum>
                                cartAdapter =
                                    ProductCartAdapter(cartData, mContext!!, this@CartActivity)
                                rvCart.layoutManager = LinearLayoutManager(mContext!!)
                                rvCart.adapter = cartAdapter
                                cartAdapter!!.notifyDataSetChanged()
                                rlCartCount.visibility = View.VISIBLE

                                initCartValue()
                            }else{
                                rlCartCount.visibility = View.GONE
                            }

                        }else{
                            rlCartCount.visibility = View.GONE
                            //mContext!!.showShortToast(response.message)
                        }
                    }
                })
        }
    }

    override fun updateProductToCart(productId: String?, variationId: String?, quantity: Int) {
        val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
        for (i in cartData.indices) {
            if (cartData[i].productId == productId && cartData[i].variationId == variationId) {
                if (cd!!.isNetworkAvailable) {
                    val cartId = cartData[i].userCartId

                    RetrofitService.getServerResponse(
                        Dialog(mContext!!),
                        retrofitApiClient!!.addUpdateCart(
                            cartId, userId, productId, quantity.toString(), variationId,
                            cartData[i].variationMarketPrice,
                            cartData[i].variationSellingPrice,
                            cartData[i].variationName,
                            cartData[i].variationQuantity
                        ),
                        object : WebResponse {
                            override fun onResponseFailed(error: String?) {
                                mContext!!.showShortToast("Server Error...")
                            }

                            override fun onResponseSuccess(result: Response<*>?) {
                                val response = result!!.body() as ResponseBody
                                val responseObject = JSONObject(response.string())
                                val status = responseObject.getInt("status")
                                val msg = responseObject.getString("message")
                                if (status == 200) {
                                    cartData[i].quantity =
                                        quantity.toString()
                                    cartAdapter!!.notifyDataSetChanged()
                                    initCartValue()
                                }else {
                                    mContext!!.showShortToast(msg)
                                }
                            }
                        })
                }

            }
        }
    }

    override fun deleteCartProduct(productId: String?, variationId: String?) {
        for (i in cartData.indices) {
            if (cartData[i].productId == productId && cartData[i].variationId == variationId) {
                val cartId = cartData[i].userCartId
                if (cd!!.isNetworkAvailable){
                    RetrofitService.getServerResponse(
                        Dialog(mContext!!),
                        retrofitApiClient!!.deleteCartProduct( cartId ),
                        object : WebResponse {
                            override fun onResponseFailed(error: String?) {
                                mContext!!.showShortToast("Server Error...")
                            }

                            override fun onResponseSuccess(result: Response<*>?) {
                                val response = result!!.body() as ResponseBody
                                val responseObject = JSONObject(response.string())
                                val status = responseObject.getInt("status")
                                val msg = responseObject.getString("message")
                                if (status == 200) {
                                    cartData.removeAt(i)
                                    cartAdapter!!.notifyDataSetChanged()
                                    initCartValue()
                                }else {
                                    mContext!!.showShortToast(msg)
                                }
                            }
                        })
                }
            }
        }
    }

    private fun initCartValue() {
        var amt = 0.0
        tvCartCount.text = cartData.size.toString()
        cartData.forEach {
            var vAmount:Double = it.variationSellingPrice.toDouble()
            var cQty:Double = it.quantity.toDouble()
            val t = vAmount*cQty
            amt += t
        }
        totalCartAmount = "%.2f".format(amt)
        tvTotalAmount.text = "₹$totalCartAmount"
        if(cartData.size>0){
            rlCartCount.visibility = View.VISIBLE
        }else{
            rlCartCount.visibility = View.GONE
        }
    }
}