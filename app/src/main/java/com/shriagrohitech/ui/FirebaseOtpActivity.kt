package com.shriagrohitech.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import com.shriagrohitech.R
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import com.chatair_android.utils.showShortToast
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.android.synthetic.main.activity_otp.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import java.util.concurrent.TimeUnit


class FirebaseOtpActivity : BaseActivity() {

    private var mCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks? = null
    private var mVerificationId: String? = null
    private var mobileNumber: String? = null
    private var serverOtp: String? = null
    private var userType: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firebase_otp)
        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        mobileNumber = intent.extras!!.getString("mobileNumber")
        userType = intent.extras!!.getString("userType")
        serverOtp = intent.extras!!.getString("serverOtp")

        lblMobile.text = "Enter the OTP that was sent to \n$mobileNumber"

        startPhoneNumberVerification()
        /*val userDa = "{\"user_id\": \"3\",\"name\": \"\",\"mobile\": \"9589306178\",\"my_referral_id\": \"BC371845\",\"profile_picture\": \"http://vintiwebsolution.com/bc/assets/user/\",\"create_date\": \"18/09/2020 02:48:58am\"}"
        AppPreference.setBooleanPreference(
            mContext!!,
            Constant.IS_LOGIN,
            true
        )
        AppPreference.setStringPreference(
            mContext!!,
            Constant.USER_DATA,
            userDa
        )
        AppPreference.setStringPreference(
            mContext!!,
            Constant.USER_ID,
            "1"
        )*/
        fabNext.setOnClickListener {
            verifyPhoneNumberWithCode()
            /*startActivity(
                Intent(mContext!!, MainActivity::class.java)
                    .putExtra("mobileNumber", "1234567890")
                    .putExtra("userId", "1")
            )*/
            //finish()
        }
        tvChangeMobile.setOnClickListener {
            startActivity(Intent(mContext!!, LoginActivity::class.java))
            finish()
        }

        /************************FIREBASE BLOCK**********************/
        mCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential)
            }

            override fun onVerificationFailed(e: FirebaseException) {}
            override fun onCodeSent(
                verificationId: String,
                forceResendingToken: PhoneAuthProvider.ForceResendingToken
            ) {
                super.onCodeSent(verificationId, forceResendingToken)
                mVerificationId = verificationId
            }
        }

    }

    private fun verifyOtp() {
        if (cd!!.isNetworkAvailable) {
            RetrofitService.getServerResponse(
                Dialog(mContext!!),
                retrofitApiClient!!.verifyOTP(mobileNumber, serverOtp), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        showToast(mContext!!, "Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as ResponseBody
                        val responseObject = JSONObject(response.string())
                        val status = responseObject.getInt("status")
                        val msg = responseObject.getString("message")
                        if (status == 200) {
                            val userData: JSONObject =
                                responseObject.getJSONObject("user_data")
                            val userId = userData.getString("user_id")
                            AppPreference.setBooleanPreference(
                                mContext!!,
                                Constant.IS_LOGIN,
                                true
                            )
                            AppPreference.setStringPreference(
                                mContext!!,
                                Constant.USER_DATA,
                                userData.toString()
                            )
                            AppPreference.setStringPreference(
                                mContext!!,
                                Constant.USER_ID,
                                userId
                            )
                            startActivity(Intent(mContext!!, MainActivity::class.java))
                            finish()
                            /*if (userType == "New") {
                                startActivity(
                                    Intent(mContext!!, ReferralActivity::class.java)
                                        .putExtra("mobileNumber", mobileNumber)
                                        .putExtra("userId", userId)
                                )
                                finish()
                            } else if (userType == "Exist") {
                                startActivity(Intent(mContext!!, MainActivity::class.java))
                                finish()
                            }*/
                        }
                        showToast(mContext!!, msg)
                    }
                })
        }
    }

    /************************************************************/
    /************************FIREBASE BLOCK**********************/
    /************************************************************/

    private fun startPhoneNumberVerification() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            "+91$mobileNumber",
            60,
            TimeUnit.SECONDS,
            this@FirebaseOtpActivity,
            object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                    signInWithPhoneAuthCredential(phoneAuthCredential)
                }

                override fun onVerificationFailed(e: FirebaseException) {
                    mContext!!.showShortToast(e.message!!)
                }
                override fun onCodeSent(
                    verificationId: String,
                    forceResendingToken: PhoneAuthProvider.ForceResendingToken
                ) {
                    super.onCodeSent(verificationId, forceResendingToken)
                    mVerificationId = verificationId
                }
            })
    }

    private fun verifyPhoneNumberWithCode() {
        val otp = pinView.value
        if (otp.length < 6) {
            showToast(mContext!!, "Enter a valid OTP")
        } else {
            val credential =
                PhoneAuthProvider.getCredential(mVerificationId!!, otp)
            signInWithPhoneAuthCredential(credential)
        }
    }

    private fun signInWithPhoneAuthCredential(phoneAuthCredential: PhoneAuthCredential) {
        FirebaseAuth.getInstance().signInWithCredential(phoneAuthCredential)
            .addOnCompleteListener(this@FirebaseOtpActivity) { task ->
                if (task.isSuccessful) {
                    val user = FirebaseAuth.getInstance().currentUser
                    if (user!=null) {
                        verifyOtp()
                    }
                }
            }
    }
}

/*{
    "status": 200,
    "message": "Correct Otp",
    "mobile": "8528528520",
    "id": "1",
    "user_data": {
        "user_id": "1",
        "name": "",
        "mobile": "8528528520",
        "gender": "",
        "image": "http://kisansahara.com/assets/user/",
        "email": "",
        "my_reffer_code": "SM7885"
    }
}

*/