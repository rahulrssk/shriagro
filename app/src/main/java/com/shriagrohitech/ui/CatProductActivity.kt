package com.shriagrohitech.ui

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chatair_android.utils.showLongToast
import com.chatair_android.utils.showShortToast
import com.infobitetechnology.samon.interfaces.CartListener
import com.infobitetechnology.samon.interfaces.SubCatClickListener
import com.shriagrohitech.R
import com.shriagrohitech.adapter.ProductAdapter
import com.shriagrohitech.adapter.ProductVariationAdapter
import com.shriagrohitech.adapter.SubCatAdapter
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.models.cart.CartModel
import com.shriagrohitech.models.cart.UserCartDatum
import com.shriagrohitech.models.cat_data.*
import com.shriagrohitech.models.home.Category
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.activity_cat_product.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

class CatProductActivity : BaseActivity(), View.OnClickListener, SubCatClickListener,
    CartListener {
    private lateinit var category: Category
    private var subCats: ArrayList<SubCategory>? = ArrayList()
    private var catProducts: ArrayList<ProductDatum>? = ArrayList()
    private var subCatProducts: ArrayList<ProductDatum>? = ArrayList()
    private var subCatAdapter: SubCatAdapter? = null
    private var productAdapter: ProductAdapter? = null
    private var isProductCat = false
    private lateinit var tvCartCount : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cat_product)

        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        tvCartCount = findViewById(R.id.tvCartCount)

        category = intent!!.getParcelableExtra("data")!!
        tvCatTitle.text = category.mainCatName
        ivBack.setOnClickListener { onBackPressed() }

    }

    override fun onStart() {
        super.onStart()
        fetchCatData()
    }

    private fun fetchCatData() {
        if (cd!!.isNetworkAvailable) {
            val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
            RetrofitService.getCatData(
                Dialog(mContext!!),
                retrofitApiClient!!.catData(category.mainCatId, userId), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as CategoryDataModel
                        if (response.status == 200) {

                            if (response.subCategory.size > 0) {
                                rvSubCat.visibility = View.VISIBLE
                                subCats = response.subCategory as ArrayList<SubCategory>?
                            } else {
                                rvSubCat.visibility = View.GONE
                            }
                            if (response.productData != null && response.productData.size > 0) {
                                catProducts = response.productData as ArrayList<ProductDatum>?
                                if (response.subCategory.size > 0) {
                                    val subCat = SubCategory()
                                    subCat.isSelected = true
                                    subCat.subCatId = "0"
                                    subCat.subCatName = category.mainCatName
                                    subCats!!.add(0, subCat)
                                }
                            }
                            cartData()

                            initData()

                        }else{
                            mContext!!.showShortToast(response.message)
                        }
                    }
                })
        }
    }

    private fun initData() {
        if (subCats!!.size > 0) {
            subCats!![0].isSelected = true
            subCatAdapter = SubCatAdapter(
                subCats!!, mContext!!,
                this@CatProductActivity
            )
            rvSubCat.layoutManager = LinearLayoutManager(
                mContext, LinearLayoutManager.HORIZONTAL,
                false
            )
            rvSubCat.adapter = subCatAdapter
            subCatAdapter!!.notifyDataSetChanged()
            initProduct(subCats!![0].subCatId)
        } else {
            initProduct("0")
        }

        btnViewCart.setOnClickListener { startActivity(Intent(mContext!!, CartActivity::class.java)) }
    }

    override fun clickedItem(subCatId: String?) {
        initProduct(subCatId)
        for (i in subCats!!.indices) {
            subCats!![i].isSelected = subCats!![i].subCatId == subCatId
            if (subCatAdapter != null) {
                subCatAdapter!!.notifyDataSetChanged()
            }
        }
    }

    private fun initProduct(subCatId: String?) {
        if (subCatId == "0") {
            rvProduct.layoutManager = LinearLayoutManager(mContext)
            productAdapter = ProductAdapter(
                catProducts!!, mContext!!,
                this@CatProductActivity, this@CatProductActivity
            )
            rvProduct.adapter = productAdapter
            productAdapter!!.notifyDataSetChanged()
            isProductCat = true
        } else {
            if (cd!!.isNetworkAvailable) {
                val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
                RetrofitService.getSubCatData(
                    Dialog(mContext!!),
                    retrofitApiClient!!.subCatData(subCatId, userId), object : WebResponse {

                        override fun onResponseFailed(error: String?) {
                            mContext!!.showShortToast("Server Error...")
                        }

                        override fun onResponseSuccess(result: Response<*>?) {
                            val response = result!!.body() as SubCatDataModel
                            subCatProducts!!.clear()
                            if (response.status == 200) {

                                if (response.productData != null && response.productData.size > 0) {
                                    subCatProducts =
                                        response.productData as ArrayList<ProductDatum>?
                                }

                            }else{
                                mContext!!.showShortToast(response.message)
                            }

                            rvProduct.layoutManager = LinearLayoutManager(mContext)
                            productAdapter = ProductAdapter(
                                subCatProducts!!, mContext!!,
                                this@CatProductActivity, this@CatProductActivity
                            )
                            rvProduct.adapter = productAdapter
                            productAdapter!!.notifyDataSetChanged()
                            isProductCat = false

                        }
                    })
            }
        }
    }

    override fun updateProductToCart(productId: String?, variationId: String?, quantity: Int) {
        val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
        if (isProductCat) {
            for (i in catProducts!!.indices) {
                if (catProducts!![i].productId == productId) {
                    for (v in catProducts!![i].productVariation.indices) {
                        if (catProducts!![i].productVariation[v].variationId == variationId) {
                            var stockCount = 0
                            try {
                                stockCount = (catProducts!![i].productVariation[v].stock).toInt()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            if (stockCount>=quantity) {
                                if (cd!!.isNetworkAvailable) {
                                    var cartId = "0"
                                    if (catProducts!![i].productVariation[v].cartId.isNotEmpty()) {
                                        cartId = catProducts!![i].productVariation[v].cartId
                                    }
                                    RetrofitService.getServerResponse(
                                        Dialog(mContext!!),
                                        retrofitApiClient!!.addUpdateCart(
                                            cartId, userId, productId, quantity.toString(), variationId,
                                            catProducts!![i].productVariation[v].stockMrp,
                                            catProducts!![i].productVariation[v].stockPrice,
                                            catProducts!![i].productVariation[v].variation,
                                            catProducts!![i].productVariation[v].variationQuantity
                                        ),
                                        object : WebResponse {
                                            override fun onResponseFailed(error: String?) {
                                                mContext!!.showShortToast("Server Error...")
                                            }

                                            override fun onResponseSuccess(result: Response<*>?) {
                                                val response = result!!.body() as ResponseBody
                                                val responseObject = JSONObject(response.string())
                                                val status = responseObject.getInt("status")
                                                val msg = responseObject.getString("message")
                                                if (status == 200) {
                                                    catProducts!![i].productVariation[v].cartQuantity =
                                                        quantity.toString()
                                                    catProducts!![i].productVariation[v].cartData =
                                                        1
                                                    productAdapter!!.notifyDataSetChanged()
                                                    cartData()
                                                }else {
                                                    mContext!!.showShortToast(msg)
                                                }
                                            }
                                        })
                                }
                            } else {
                                if (stockCount==0){
                                    mContext!!.showLongToast("Product is Out of Stock")
                                }else{
                                    mContext!!.showLongToast("Stock is Limited")
                                }
                            }
                        }
                    }
                }
            }
        } else {
            for (i in subCatProducts!!.indices) {
                if (subCatProducts!![i].productId == productId) {
                    for (v in subCatProducts!![i].productVariation.indices) {
                        if (subCatProducts!![i].productVariation[v].variationId == variationId) {
                            var stockCount = 0
                            try {
                                stockCount = (catProducts!![i].productVariation[v].stock).toInt()
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                            if (stockCount>=quantity) {
                                if (cd!!.isNetworkAvailable) {
                                    var cartId = "0"
                                    if (subCatProducts!![i].productVariation[v].cartId.isNotEmpty()) {
                                        cartId = subCatProducts!![i].productVariation[v].cartId
                                    }
                                    RetrofitService.getServerResponse(
                                        Dialog(mContext!!),
                                        retrofitApiClient!!.addUpdateCart(
                                            cartId, userId, productId, quantity.toString(), variationId,
                                            subCatProducts!![i].productVariation[v].stockMrp,
                                            subCatProducts!![i].productVariation[v].stockPrice,
                                            subCatProducts!![i].productVariation[v].variation,
                                            subCatProducts!![i].productVariation[v].variationQuantity
                                        ),
                                        object : WebResponse {
                                            override fun onResponseFailed(error: String?) {
                                                mContext!!.showShortToast("Server Error...")
                                            }

                                            override fun onResponseSuccess(result: Response<*>?) {
                                                val response = result!!.body() as ResponseBody
                                                val responseObject = JSONObject(response.string())
                                                val status = responseObject.getInt("status")
                                                val msg = responseObject.getString("message")
                                                if (status == 200) {
                                                    subCatProducts!![i].productVariation[v].cartQuantity =
                                                        quantity.toString()
                                                    subCatProducts!![i].productVariation[v].cartData =
                                                        1
                                                    productAdapter!!.notifyDataSetChanged()
                                                    cartData()
                                                }else {
                                                    mContext!!.showShortToast(msg)
                                                }
                                            }
                                        })
                                }
                            } else {
                                if (stockCount==0){
                                    mContext!!.showLongToast("Product is Out of Stock")
                                }else{
                                    mContext!!.showLongToast("Stock is Limited")
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun deleteCartProduct(productId: String?, variationId: String?) {
        if (isProductCat) {
            for (i in catProducts!!.indices) {
                if (catProducts!![i].productId == productId) {
                    for (v in catProducts!![i].productVariation.indices) {
                        if (catProducts!![i].productVariation[v].variationId == variationId) {

                            if (cd!!.isNetworkAvailable) {
                                var cartId = "0"
                                if (catProducts!![i].productVariation[v].cartId.isNotEmpty()) {
                                    cartId = catProducts!![i].productVariation[v].cartId
                                }
                                RetrofitService.getServerResponse(
                                    Dialog(mContext!!),
                                    retrofitApiClient!!.deleteCartProduct( cartId ),
                                    object : WebResponse {
                                        override fun onResponseFailed(error: String?) {
                                            mContext!!.showShortToast("Server Error...")
                                        }

                                        override fun onResponseSuccess(result: Response<*>?) {
                                            val response = result!!.body() as ResponseBody
                                            val responseObject = JSONObject(response.string())
                                            val status = responseObject.getInt("status")
                                            val msg = responseObject.getString("message")
                                            if (status == 200) {
                                                catProducts!![i].productVariation[v].cartQuantity = "0"
                                                catProducts!![i].productVariation[v].cartData = 0
                                                catProducts!![i].productVariation[v].cartId = "0"
                                                productAdapter!!.notifyDataSetChanged()
                                                cartData()
                                            }else {
                                                mContext!!.showShortToast(msg)
                                            }
                                        }
                                    })
                            }
                        }

                    }
                }
            }
        } else {
            for (i in subCatProducts!!.indices) {
                if (subCatProducts!![i].productId == productId) {
                    for (v in subCatProducts!![i].productVariation.indices) {
                        if (subCatProducts!![i].productVariation[v].variationId == variationId) {

                            if (cd!!.isNetworkAvailable) {
                                var cartId = "0"
                                if (subCatProducts!![i].productVariation[v].cartId.isNotEmpty()) {
                                    cartId = subCatProducts!![i].productVariation[v].cartId
                                }
                                RetrofitService.getServerResponse(
                                    Dialog(mContext!!),
                                    retrofitApiClient!!.deleteCartProduct( cartId ),
                                    object : WebResponse {
                                        override fun onResponseFailed(error: String?) {
                                            mContext!!.showShortToast("Server Error...")
                                        }

                                        override fun onResponseSuccess(result: Response<*>?) {
                                            val response = result!!.body() as ResponseBody
                                            val responseObject = JSONObject(response.string())
                                            val status = responseObject.getInt("status")
                                            val msg = responseObject.getString("message")
                                            if (status == 200) {
                                                subCatProducts!![i].productVariation[v].cartQuantity = "0"
                                                subCatProducts!![i].productVariation[v].cartData = 0
                                                subCatProducts!![i].productVariation[v].cartId = "0"
                                                productAdapter!!.notifyDataSetChanged()
                                                cartData()
                                            }else {
                                                mContext!!.showShortToast(msg)
                                            }
                                        }
                                    })
                            }
                        }

                    }
                }
            }
        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.rlVariations -> {
                val tag = p0.tag as Int
                if (isProductCat){
                    if (catProducts!![tag].productVariation.size>0) {
                        openVariationDialog(tag)
                    }
                }else{
                    if (subCatProducts!![tag].productVariation.size>0) {
                        openVariationDialog(tag)
                    }
                }
            }
        }
    }

    private fun openVariationDialog(position: Int) {
        val dialogBox = AlertDialog.Builder(mContext)
        dialogBox.setCancelable(false)

        val li = LayoutInflater.from(mContext)
        val view: View = li.inflate(R.layout.dialog_select_poduct_variation, null)
        dialogBox.setView(view)
        val alertDialog = dialogBox.create()
        alertDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        val rvProductType = view.findViewById<RecyclerView>(R.id.rvProductType)
        rvProductType.layoutManager = LinearLayoutManager(mContext!!)
        val variationAdapter : ProductVariationAdapter

        if (isProductCat){
            variationAdapter = ProductVariationAdapter(
                catProducts!![position].productVariation as ArrayList<ProductVariation>,
                mContext!!, View.OnClickListener {
                    val tag = it.tag as Int
                    for( i in catProducts!![position].productVariation.indices){
                        catProducts!![position].productVariation[i].isSelected = i==tag
                        alertDialog.dismiss()
                        productAdapter!!.notifyDataSetChanged()
                    }
                }, 0)
        }else{
            variationAdapter = ProductVariationAdapter(
                subCatProducts!![position].productVariation as ArrayList<ProductVariation>,
                mContext!!, View.OnClickListener {
                    val tag = it.tag as Int
                    for( i in subCatProducts!![position].productVariation.indices){
                        subCatProducts!![position].productVariation[i].isSelected = i==tag
                        alertDialog.dismiss()
                        productAdapter!!.notifyDataSetChanged()
                    }
                }, 0)
        }
        rvProductType.adapter = variationAdapter
        variationAdapter.notifyDataSetChanged()
        alertDialog.show()
    }

    private fun cartData() {
        if (cd!!.isNetworkAvailable) {
            val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
            RetrofitService.cartDataResponse(
                Dialog(mContext!!),
                retrofitApiClient!!.cartData(userId), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as CartModel
                        if (response.status == 200) {

                            if (response.userCartData.size > 0) {
                                val cartCount = response.userCartData.size
                                tvCartCount.setText(cartCount.toString())
                                val cartData = response.userCartData as ArrayList<UserCartDatum>
                                var amt = 0.0
                                cartData.forEach {
                                    var vAmount: Double = it.variationSellingPrice.toDouble()
                                    var cQty: Double = it.quantity.toDouble()
                                    val t = vAmount * cQty
                                    amt += t
                                }
                                /*"%.2f".format(vsPrice)*/
                                val ta = "%.2f".format(amt)
                                tvTotalAmount.text = "₹$ta"
                                rlCartCount.visibility = View.VISIBLE
                            }else{
                                rlCartCount.visibility = View.GONE
                            }


                        }else{
                            rlCartCount.visibility = View.GONE
                            //mContext!!.showShortToast(response.message)
                        }
                    }
                })
        }
    }
}