package com.shriagrohitech.ui

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chatair_android.utils.showLongToast
import com.chatair_android.utils.showShortToast
import com.shriagrohitech.R
import com.shriagrohitech.adapter.ProductVariationAdapter
import com.shriagrohitech.adapter.RelatedProductListAdapter
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.models.cart.CartModel
import com.shriagrohitech.models.cart.UserCartDatum
import com.shriagrohitech.models.cat_data.ProductDatum
import com.shriagrohitech.models.cat_data.ProductDetailModel
import com.shriagrohitech.models.cat_data.ProductVariation
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_product_detail.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

class ProductDetailActivity : BaseActivity() {

    var productData: ProductDatum? = null
    var productId = ""
    private lateinit var tvCartCount : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        productId = intent.extras!!.getString("productId")!!
        val productTitle = intent.extras!!.getString("productName")
        tvCatTitle.text = productTitle

        ivBack.setOnClickListener { onBackPressed() }

        tvCartCount = findViewById(R.id.tvCartCount)
    }

    override fun onStart() {
        super.onStart()
        fetchProductDetail(productId)

    }

    private fun fetchProductDetail(productId: String?) {
        if (cd!!.isNetworkAvailable) {
            val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
            RetrofitService.singleProductDetail(
                Dialog(mContext!!),
                retrofitApiClient!!.singleProductDetail(productId, userId), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as ProductDetailModel
                        if (response.status == 200) {
                            if (response.productData != null) {
                                productData = response.productData
                                initProductData(true)
                            }
                            if (response.relatedProduct.size>0){
                                rvRelatedProduct.layoutManager =
                                    LinearLayoutManager(
                                        mContext!!,
                                        LinearLayoutManager.HORIZONTAL, false
                                    )
                                rvRelatedProduct.adapter = RelatedProductListAdapter(
                                    response.relatedProduct as ArrayList<ProductDatum>,
                                    mContext!!, this@ProductDetailActivity,
                                    "details")
                            }
                        }
                    }
                })
        }
        //singleProductDetail
    }

    private fun initProductData(isRefreshCart : Boolean) {
        Glide.with(mContext!!)
            .load(productData!!.productImage)
            .fitCenter()
            .placeholder(R.drawable.gallery)
            .error(R.drawable.gallery)
            .into(ivProductImage)
        tvProductName.text = productData!!.productName
        tvProductDetail.text = productData!!.productDescription
        if (productData!!.productVariation.size > 0) {
            var selected = false
            productData!!.productVariation.forEach {
                if (it.isSelected) {
                    selected = true
                    tvVariations.text = "${it.variationQuantity} " +
                            "${it.variation}"

                    var vsPrice = 0.0
                    var marketPrice = 0.0

                    try {
                        vsPrice = (it.stockPrice).toDouble()
                        marketPrice = (it.stockMrp).toDouble()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    val variationSellingPrice = "%.2f".format(vsPrice)
                    val variationMarketPrice = "%.2f".format(marketPrice)

                    tvPrice.text = "Price : ₹$variationSellingPrice"

                    var stockCount = 0
                    try {
                        stockCount = (it.stock).toInt()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (stockCount>0) {
                        tvStock.text = "In Stock"
                        tvStock.setTextColor(ContextCompat.getColor(mContext!!, R.color.colorPrimary))
                    } else {
                        tvStock.text = "Out of Stock"
                        tvStock.setTextColor(ContextCompat.getColor(mContext!!, R.color.colorPr1))
                    }

                    tvMrp.text = "MRP : ₹$variationMarketPrice"
                    tvMrp.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                    if (it.cartData > 0) {
                        rlCartQuantity.visibility = View.VISIBLE
                        tvAddToCart.visibility = View.GONE
                        tvCartCountt.text = "${it.cartQuantity}"
                    } else {
                        rlCartQuantity.visibility = View.GONE
                        tvAddToCart.visibility = View.VISIBLE
                    }
                }
            }
            if (!selected) {
                tvVariations.text = "${productData!!.productVariation[0].variationQuantity} " +
                        "${productData!!.productVariation[0].variation}"

                var vsPrice = 0.0
                var marketPrice = 0.0

                try {
                    vsPrice = (productData!!.productVariation[0].stockPrice).toDouble()
                    marketPrice = (productData!!.productVariation[0].stockMrp).toDouble()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                val variationSellingPrice = "%.2f".format(vsPrice)
                val variationMarketPrice = "%.2f".format(marketPrice)

                tvPrice.text = "Price : ₹$variationSellingPrice"

                var stockCount = 0
                try {
                    stockCount = (productData!!.productVariation[0].stock).toInt()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (stockCount>0) {
                    tvStock.text = "In Stock"
                    tvStock.setTextColor(ContextCompat.getColor(mContext!!, R.color.colorPrimary))
                } else {
                    tvStock.text = "Out of Stock"
                    tvStock.setTextColor(ContextCompat.getColor(mContext!!, R.color.colorPr1))
                }

                tvMrp.text = "MRP : ₹$variationMarketPrice"
                tvMrp.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                if (productData!!.productVariation[0].cartData > 0) {
                    rlCartQuantity.visibility = View.VISIBLE
                    tvAddToCart.visibility = View.GONE
                    tvCartCountt.text = "${productData!!.productVariation[0].cartQuantity}"
                } else {
                    rlCartQuantity.visibility = View.GONE
                    tvAddToCart.visibility = View.VISIBLE
                }
            }
        }

        rlVariations.setOnClickListener {
            openVariationDialog()
        }

        tvAddToCart.setOnClickListener {
            if (productData!!.productVariation.size > 0) {
                var isVariationSelected = false
                productData!!.productVariation.forEach {
                    if (it.isSelected) {
                        isVariationSelected = true
                        updateProductToCart(it.productId, it.variationId, 1)
                    }
                }
                if (!isVariationSelected) {
                    updateProductToCart(
                        productData!!.productVariation[0].productId,
                        productData!!.productVariation[0].variationId, 1
                    )
                }
            }
        }
        ivPlusCart.setOnClickListener {
            if (productData!!.productVariation.size > 0) {
                var isVariationSelected = false
                productData!!.productVariation.forEach {
                    if (it.isSelected) {
                        isVariationSelected = true
                        val qty: Int = (it.cartQuantity.toInt()) + 1
                        updateProductToCart(it.productId, it.variationId, qty)
                    }
                }
                if (!isVariationSelected) {
                    val qty = (productData!!.productVariation[0].cartQuantity.toInt()) + 1
                    updateProductToCart(
                        productData!!.productVariation[0].productId,
                        productData!!.productVariation[0].variationId, qty
                    )
                }
            }
        }
        ivMinusCart.setOnClickListener {
            if (productData!!.productVariation.size > 0) {
                var isVariationSelected = false
                productData!!.productVariation.forEach {
                    if (it.isSelected) {
                        isVariationSelected = true
                        val qt = (it.cartQuantity.toInt())
                        var qty = 0
                        if (qt > 0) {
                            qty = qt - 1
                        }
                        if (qty > 0) {
                            updateProductToCart(it.productId, it.variationId, qty)
                        } else {
                            deleteCartProduct(it.productId, it.variationId)
                        }
                    }
                }
                if (!isVariationSelected) {
                    val qt = (productData!!.productVariation[0].cartQuantity.toInt())
                    var qty = 0
                    if (qt > 0) {
                        qty = qt - 1
                    }
                    if (qty > 0) {
                        updateProductToCart(
                            productData!!.productVariation[0].productId,
                            productData!!.productVariation[0].variationId, qty
                        )
                    } else {
                        deleteCartProduct(
                            productData!!.productVariation[0].productId,
                            productData!!.productVariation[0].variationId
                        )
                    }
                }
            }
        }
        if (isRefreshCart) {
            cartData()
        }

        btnViewCart.setOnClickListener { startActivity(Intent(mContext!!, CartActivity::class.java)) }
    }

    private fun deleteCartProduct(productId: String?, variationId: String?) {
        for (v in productData!!.productVariation.indices) {
            if (productData!!.productVariation[v].variationId == variationId) {

                if (cd!!.isNetworkAvailable) {
                    var cartId = "0"
                    if (productData!!.productVariation[v].cartId.isNotEmpty()) {
                        cartId = productData!!.productVariation[v].cartId
                    }
                    RetrofitService.getServerResponse(
                        Dialog(mContext!!),
                        retrofitApiClient!!.deleteCartProduct(cartId),
                        object : WebResponse {
                            override fun onResponseFailed(error: String?) {
                                mContext!!.showShortToast("Server Error...")
                            }

                            override fun onResponseSuccess(result: Response<*>?) {
                                val response = result!!.body() as ResponseBody
                                val responseObject = JSONObject(response.string())
                                val status = responseObject.getInt("status")
                                val msg = responseObject.getString("message")
                                if (status == 200) {
                                    productData!!.productVariation[v].cartQuantity = "0"
                                    productData!!.productVariation[v].cartData = 0
                                    productData!!.productVariation[v].cartId = "0"
                                    initProductData(true)
                                }else {
                                    mContext!!.showShortToast(msg)
                                }
                            }
                        })
                }
            }

        }
    }

    private fun updateProductToCart(productId: String?, variationId: String?, quantity: Int) {
        val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
        for (v in productData!!.productVariation.indices) {
            if (productData!!.productVariation[v].variationId == variationId) {
                var stockCount = 0
                try {
                    stockCount = (productData!!.productVariation[v].stock).toInt()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if (stockCount>=quantity) {
                    if (cd!!.isNetworkAvailable) {
                        var cartId = "0"
                        if (productData!!.productVariation[v].cartId.isNotEmpty()) {
                            cartId = productData!!.productVariation[v].cartId
                        }
                        RetrofitService.getServerResponse(
                            Dialog(mContext!!),
                            retrofitApiClient!!.addUpdateCart(
                                cartId, userId, productId, quantity.toString(), variationId,
                                productData!!.productVariation[v].stockMrp,
                                productData!!.productVariation[v].stockPrice,
                                productData!!.productVariation[v].variation,
                                productData!!.productVariation[v].variationQuantity
                            ),
                            object : WebResponse {
                                override fun onResponseFailed(error: String?) {
                                    mContext!!.showShortToast("Server Error...")
                                }

                                override fun onResponseSuccess(result: Response<*>?) {
                                    val response = result!!.body() as ResponseBody
                                    val responseObject = JSONObject(response.string())
                                    val status = responseObject.getInt("status")
                                    val msg = responseObject.getString("message")
                                    if (status == 200) {
                                        productData!!.productVariation[v].cartQuantity =
                                            quantity.toString()
                                        productData!!.productVariation[v].cartData =
                                            1
                                        initProductData(true)
                                    }else {
                                        mContext!!.showShortToast(msg)
                                    }
                                }
                            })
                    }
                } else {
                    if (stockCount==0){
                        mContext!!.showLongToast("Product is Out of Stock")
                    }else{
                        mContext!!.showLongToast("Product Stock is Limited")
                    }
                }
            }
        }
    }

    private fun cartData() {
        if (cd!!.isNetworkAvailable) {
            val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
            RetrofitService.cartDataResponse(
                Dialog(mContext!!),
                retrofitApiClient!!.cartData(userId), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as CartModel
                        if (response.status == 200) {

                            if (response.userCartData.size > 0) {
                                val cartData = response.userCartData as ArrayList<UserCartDatum>
                                tvCartCount.text = cartData.size.toString()
                                var amt = 0.0
                                cartData.forEach {
                                    var vAmount: Double = it.variationSellingPrice.toDouble()
                                    var cQty: Double = it.quantity.toDouble()
                                    val t = vAmount * cQty
                                    amt += t
                                }
                                val ta = "%.2f".format(amt)
                                tvTotalAmount.text = "₹$ta"
                                rlCartCount.visibility = View.VISIBLE
                            }else{
                                rlCartCount.visibility = View.GONE
                            }


                        }else{
                            rlCartCount.visibility = View.GONE
                            //mContext!!.showShortToast(response.message)
                        }
                    }
                })
        }
    }

    private fun openVariationDialog() {
        val dialogBox = AlertDialog.Builder(mContext)
        dialogBox.setCancelable(false)

        val li = LayoutInflater.from(mContext)
        val view: View = li.inflate(R.layout.dialog_select_poduct_variation, null)
        dialogBox.setView(view)
        val alertDialog = dialogBox.create()
        alertDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        val rvProductType = view.findViewById<RecyclerView>(R.id.rvProductType)
        rvProductType.layoutManager = LinearLayoutManager(mContext!!)
        val variationAdapter: ProductVariationAdapter

        variationAdapter = ProductVariationAdapter(
            productData!!.productVariation as ArrayList<ProductVariation>,
            mContext!!, View.OnClickListener {
                val tag = it.tag as Int
                for (i in productData!!.productVariation.indices) {
                    productData!!.productVariation[i].isSelected = i == tag
                    alertDialog.dismiss()
                    initProductData(false)
                }
            }, 0
        )

        rvProductType.adapter = variationAdapter
        variationAdapter.notifyDataSetChanged()
        alertDialog.show()
    }
}