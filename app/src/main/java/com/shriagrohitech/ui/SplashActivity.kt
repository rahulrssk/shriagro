package com.shriagrohitech.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.shriagrohitech.R
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()
        //FirebaseApp.initializeApp(this)

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val window: Window = this.window
            //val background = resources.getDrawable(R.drawable.background_image)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            //window.statusBarColor = resources.getColor(android.R.color.white)
            window.statusBarColor = resources.getColor(R.color.background_screen)
            //window.navigationBarColor = this.resources.getColor(android.R.color.transparent)//bottom bar color
            //window.setBackgroundDrawable(background)
        }*/

       // checkLocationPermission()

        Handler().postDelayed({
            val isLogin = AppPreference.getBooleanPreference(mContext!!, Constant.IS_LOGIN)
            if (isLogin) {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }, 2000)

    }

    /*private fun checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            permissionDialog()
            return
        } else {
            init()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                init()
            } else {
                permissionDialog()
            }
        } else {
            permissionDialog()
        }
    }

    private fun permissionDialog() {

        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            1
        )

    }

    private fun init() {
      //  Glide.with(this).load(R.drawable.logo).into(imgMyCart)

        Handler().postDelayed({
            if (AppPreference.getBooleanPreference(this, Constant.IS_LOGIN)) {
                val responseObject = AppPreference.getStringPreference(mContext, Constant.USER_LOGIN_DATA)
                val gson = Gson()
                val loginModel  = gson.fromJson(responseObject.toString(), UserLoginModel::class.java)
                UserLoginModel.setUserLoginModel(loginModel)
                startActivity(Intent(mContext, MainActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }
        }, 2000)

    }*/
}
