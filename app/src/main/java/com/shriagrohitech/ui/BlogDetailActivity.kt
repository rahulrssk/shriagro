package com.shriagrohitech.ui

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.text.Html
import com.bumptech.glide.Glide
import com.chatair_android.utils.showShortToast
import com.shriagrohitech.R
import com.shriagrohitech.models.home.Blog
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.activity_blog_detail.*
import kotlinx.android.synthetic.main.activity_blog_detail.ivBack
import kotlinx.android.synthetic.main.activity_blog_detail.tvTitle
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

class BlogDetailActivity : BaseActivity() {
    private lateinit var blog : Blog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_blog_detail)

        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()
        blog = intent.getParcelableExtra("data")!!

        tvTitle.text = blog.title
        ivBack.setOnClickListener { onBackPressed() }

        Glide.with(mContext!!)
            .load(blog.image)
            .centerCrop()
            .placeholder(R.drawable.gallery)
            .error(R.drawable.gallery)
            .into(ivBlog)

        fetchBlog(blog.id)

    }

    private fun fetchBlog(id: String?) {
        if (cd!!.isNetworkAvailable){
            RetrofitService.getServerResponse(
                Dialog(mContext!!), retrofitApiClient!!.getBlogDetails(id), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as ResponseBody
                        val responseObject = JSONObject(response.string())
                        val status = responseObject.getInt("status")
                        val msg = responseObject.getString("message")

                        if (status == 200) {
                            val blogData: JSONObject = responseObject.getJSONArray("blog_data")[0] as JSONObject
                            val description = blogData.getString("description")
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                tvBlogContent.text = Html.fromHtml(description, Html.FROM_HTML_MODE_LEGACY);
                            } else {
                                tvBlogContent.text = Html.fromHtml(description);
                            }
                        }else {
                            mContext!!.showShortToast(msg)
                        }
                    }
                })
        }
    }
}