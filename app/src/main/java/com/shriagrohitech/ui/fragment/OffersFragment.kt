package com.shriagrohitech.ui.fragment

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.chatair_android.utils.showShortToast
import com.shriagrohitech.R
import com.shriagrohitech.adapter.CouponsAdapter
import com.shriagrohitech.models.offers.OffersModel
import com.shriagrohitech.models.offers.PromotionList
import com.shriagrohitech.retrofit.RetrofitApiClient
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.fragment_my_matches.*
import retrofit2.Response

class OffersFragment : Fragment() {

    var retrofitApiClient: RetrofitApiClient? = null
    var cd: ConnectionDetector? = null
    var mContext: Context? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_matches, container, false)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        mContext = activity
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()
        initOffers()
    }

    private fun initOffers() {
        if (cd!!.isNetworkAvailable){
            RetrofitService.offersResponse(
                Dialog(mContext!!), retrofitApiClient!!.offers(),
                object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as OffersModel
                        if(response.status==200){
                            if (response.promotionList.size>0) {
                                val offers = response.promotionList as ArrayList<PromotionList>
                                rvMyOrders.layoutManager = LinearLayoutManager(mContext)
                                rvMyOrders.adapter = CouponsAdapter(offers, mContext!!)
                            }
                        }
                    }
                })
        }
    }
}