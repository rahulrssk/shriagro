package com.shriagrohitech.ui.fragment

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.shriagrohitech.R
import com.shriagrohitech.retrofit.RetrofitApiClient
import com.shriagrohitech.ui.AboutContentActivity
import com.shriagrohitech.ui.LoginActivity
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.ConnectionDetector

class MoreFragment : Fragment() {

    var retrofitApiClient: RetrofitApiClient? = null
    var cd: ConnectionDetector? = null
    var mContext: Context? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_more, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mContext = activity
        cd = ConnectionDetector(mContext)
        view.findViewById<TextView>(R.id.tvLogout).setOnClickListener {
            doLogout()
        }
        view.findViewById<TextView>(R.id.tvTC).setOnClickListener {
            startActivity(
                Intent(mContext, AboutContentActivity::class.java)
                    .putExtra("from", "tc")
            )
        }
        view.findViewById<TextView>(R.id.tvAboutUs).setOnClickListener {
            startActivity(
                Intent(mContext, AboutContentActivity::class.java)
                    .putExtra("from", "about")
            )
        }
        view.findViewById<TextView>(R.id.tvPrivacyPolicy).setOnClickListener {
            startActivity(
                Intent(mContext, AboutContentActivity::class.java)
                    .putExtra("from", "privacy")
            )
        }
    }

    private fun doLogout() {
        val dialog = AlertDialog.Builder(mContext!!, R.style.AlertDialogTheme)
        dialog
            .setTitle("Logout")
            .setMessage("Are you sure want to Logout ?")
            .setPositiveButton(
                "YES"
            ) { _: DialogInterface?, _: Int ->
                FirebaseAuth.getInstance().signOut()
                AppPreference.clearAllPreferences(mContext)
                val intent = Intent(mContext, LoginActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                activity!!.finish()
            }
            .setNegativeButton("NO", null)
            .create()
            .show()
    }
}