package com.shriagrohitech.ui.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.chatair_android.utils.showShortToast
import com.github.dhaval2404.imagepicker.ImagePicker
import com.shriagrohitech.R
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.retrofit.RetrofitApiClient
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.ConnectionDetector
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response
import java.io.File

class AccountFragment : Fragment() {

    private var ivProfileImage: CircleImageView? = null
    var retrofitApiClient: RetrofitApiClient? = null
    var cd: ConnectionDetector? = null
    var mContext: Context? = null
    private var etContact: EditText? = null
    private var etEmail: EditText? = null
    private var etFullName: EditText? = null
    private var etVillage: EditText? = null
    private var etTehshil: EditText? = null
    private var etPincode: EditText? = null
    private var etCity: EditText? = null
    private var file: File? = null
    private var genderArray = arrayOf("Select Gender", "Male", "Female")
    private var selectedGender = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mContext = activity
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()
        val v = inflater.inflate(R.layout.fragment_account, container, false)
        initView(v)
        return v
    }

    private fun initView(view: View) {
        val ivAddProfile = view.findViewById<ImageView>(R.id.ivAddProfile)
        etFullName = view.findViewById(R.id.etFullName)
        etEmail = view.findViewById(R.id.etEmail)
        etContact = view.findViewById(R.id.etContact)
        etVillage = view.findViewById(R.id.etVillage)
        etTehshil = view.findViewById(R.id.etTehshil)
        etCity = view.findViewById(R.id.etCity)
        etPincode = view.findViewById(R.id.etPincode)
        ivProfileImage = view.findViewById(R.id.ivProfileImage)
        val spnGender = view.findViewById<Spinner>(R.id.spnGender)

        /*"user_data": {
        "user_id": "1",
        "name": "",
        "mobile": "8528528520",
        "gender": "",
        "image": "http://kisansahara.com/assets/user/",
        "email": "",
        "my_reffer_code": "SM7885",
    "gaon": "Village",
    "tehsil": "Tehsil",
    "city": "City",
    "pincode": "886688"
    }*/

        /*update_user_profile
user_id, name, mobile, email, gender, gaon, tehsil, city, pincode*/

        val userObject =
            JSONObject(AppPreference.getStringPreference(mContext!!, Constant.USER_DATA))

        etFullName!!.setText(userObject.getString("name"))
        etContact!!.setText(userObject.getString("mobile"))
        etEmail!!.setText(userObject.getString("email"))
        val gender = try {
            etVillage!!.setText(userObject.getString("gaon"))
            etTehshil!!.setText(userObject.getString("tehsil"))
            etCity!!.setText(userObject.getString("city"))
            etPincode!!.setText(userObject.getString("pincode"))
            userObject.getString("gender")
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val userID = userObject.getString("user_id")

        Glide.with(mContext!!)
            .load(userObject.getString("image"))
            .placeholder(R.drawable.ic_profile)
            .error(R.drawable.ic_profile)
            .into(ivProfileImage!!)

        val genderAdapter =
            ArrayAdapter(mContext!!, R.layout.simple_spinner_item, genderArray)
        spnGender.adapter = genderAdapter
        try {
            if (genderArray.contains(gender)){
                spnGender.setSelection(genderArray.indexOf(gender))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        spnGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position == 0) {
                    selectedGender = ""
                } else {
                    selectedGender = genderArray[position]
                }
            }
        }
        genderAdapter.notifyDataSetChanged()

        ivAddProfile.setOnClickListener {
            ImagePicker.with(this)
                .cropSquare()                    //Crop image(Optional), Check Customization for more option
                .compress(512)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(
                    1080,
                    1080
                )    //Final image resolution will be less than 1080 x 1080(Optional)
                .start()
        }

        val btnSave = view.findViewById<Button>(R.id.btnSave)
        btnSave.setOnClickListener {
            val name = etFullName!!.text.toString().trim()
            val email = etEmail!!.text.toString().trim()
            val village = etVillage!!.text.toString().trim()
            val tehsil = etTehshil!!.text.toString().trim()
            val city = etCity!!.text.toString().trim()
            val pincode = etPincode!!.text.toString().trim()
            if (name.isEmpty()) {
                mContext!!.showShortToast("Name should not be empty...")
            } else if (selectedGender.isEmpty()){
                mContext!!.showShortToast("Select a Gender...")
            } else if (village.isEmpty()){
                mContext!!.showShortToast("Village Name should not be empty...")
            } else if (tehsil.isEmpty()){
                mContext!!.showShortToast("Tehsil Name should not be empty...")
            } else if (city.isEmpty()){
                mContext!!.showShortToast("City Name should not be empty...")
            } else if (pincode.isEmpty()){
                mContext!!.showShortToast("Pincode Name should not be empty...")
            } else {
                val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
                if (cd!!.isNetworkAvailable) {
                    RetrofitService.getServerResponse(
                        Dialog(mContext!!),
                        retrofitApiClient!!.updateUser(userId, name, selectedGender, email,
                            village, tehsil, city, pincode),
                        object : WebResponse {
                            override fun onResponseFailed(error: String?) {
                                mContext!!.showShortToast("Server Error...")
                            }

                            override fun onResponseSuccess(result: Response<*>?) {
                                val response = result!!.body() as ResponseBody
                                val responseObject = JSONObject(response.string())
                                val status = responseObject.getInt("status")
                                val msg = responseObject.getString("message")
                                if (status == 200) {
                                    val userDataObject =
                                        responseObject.getJSONObject("user_profile")
                                    AppPreference.setStringPreference(
                                        mContext!!,
                                        Constant.USER_DATA,
                                        userDataObject.toString()
                                    )
                                    AppPreference.setBooleanPreference(
                                        mContext!!,
                                        Constant.IS_COMPLETE_ADDRESS,
                                        true
                                    )
                                }
                                mContext!!.showShortToast(msg)
                            }
                        })
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            //You can get File object from intent
            file = ImagePicker.getFile(data)!!

            val myBitmap: Bitmap = BitmapFactory.decodeFile(file!!.absolutePath)
            val fileUri = data?.data
            ivProfileImage!!.setImageBitmap(myBitmap)

            updateUserProfilePicture(file!!)

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(mContext!!, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(mContext!!, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateUserProfilePicture(file: File) {
        val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
        val uId: RequestBody = userId.toRequestBody("text/plain".toMediaTypeOrNull())
        val uIamgeP = file.asRequestBody("image/*".toMediaTypeOrNull())
        var userProfilePart: MultipartBody.Part =
            MultipartBody.Part.createFormData("image", file.name, uIamgeP)
        if (cd!!.isNetworkAvailable) {
            RetrofitService.getServerResponse(
                Dialog(mContext!!),
                retrofitApiClient!!.updateUserProfile(uId, userProfilePart),
                object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as ResponseBody
                        val responseObject = JSONObject(response.string())
                        val status = responseObject.getInt("status")
                        val msg = responseObject.getString("message")
                        if (status == 200) {
                            val userDataObject = responseObject.getJSONObject("user_profile")
                            AppPreference.setStringPreference(
                                mContext!!,
                                Constant.USER_DATA,
                                userDataObject.toString()
                            )
                            val pic = userDataObject.getString("image")
                            Glide.with(mContext!!)
                                .load(pic)
                                .placeholder(R.drawable.placeholder_square)
                                .error(R.drawable.placeholder_square)
                                .into(ivProfileImage!!)
                        }
                        mContext!!.showShortToast(msg)
                    }
                })
        }
    }
}

/*{
  "status": 200,
  "message": "Sucess",
  "user_profile": {
    "user_id": "5",
    "name": "John",
    "mobile": "8528528520",
    "profile_picture": "http://vintiwebsolution.com/bc/assets/user/IMG_20201015_095931616.jpg",
    "email": "john@mail.com",
    "my_referral_id": "BC610443"
  }
}
*/

/*{
    "status": 200,
    "message": "Login....",
    "mobile_number": "9589306178",
    "user_details": [
        {
            "user_id": "3",
            "name": "",
            "mobile": "9589306178",
            "my_referral_id": "BC371845",
            "profile_picture": "http://vintiwebsolution.com/bc/assets/user/",
            "create_date": "18/09/2020 02:48:58am"
        }
    ]
}

{
  "status": 200,
  "message": "Sucess",
  "user_profile": {
    "user_id": "5",
    "name": "John",
    "mobile": "8528528520",
    "image": "http://vintiwebsolution.com/bc/assets/user/",
    "email": "john@mail.com",
    "referral_id": "BC610443"
  }
}

 "user_data": {
        "user_id": "3",
        "name": "",
        "mobile": "9589306178",
        "my_referral_id": "BC371845",
        "profile_picture": "http://vintiwebsolution.com/bc/assets/user/",
        "wallet_amount": 30,
        "create_date": "18/09/2020 02:48:58am"
    }

{"status":400,"message":"Wrong OTP","mobile_number":"9589306178"}

*/