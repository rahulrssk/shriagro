package com.shriagrohitech.ui.fragment

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.chatair_android.utils.showShortToast
import com.infobitetechnology.samon.interfaces.CartListener
import com.infobitetechnology.samon.interfaces.SubCatClickListener
import com.shriagrohitech.R
import com.shriagrohitech.adapter.*
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.models.cat_data.ProductDatum
import com.shriagrohitech.models.home.*
import com.shriagrohitech.retrofit.RetrofitApiClient
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.ConnectionDetector
import com.shriagrohitech.utils.FixedSpeedScroller
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Response
import java.lang.reflect.Field
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : Fragment(), View.OnClickListener, SubCatClickListener,
    CartListener {

    var retrofitApiClient: RetrofitApiClient? = null
    var cd: ConnectionDetector? = null
    var mContext: Context? = null
    private var cp1 = 0
    private var swipeTimer: Timer? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        mContext = activity
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()
        //homeContent()

    }

    override fun onStart() {
        super.onStart()
        homeContent()
    }

    private fun initHomeCat(
        categories: ArrayList<Category>,
        blogs: ArrayList<Blog>,
        tProducts: ArrayList<ProductDatum>
    ) {
        rvCategory.layoutManager = GridLayoutManager(mContext, 4)
        rvCategory.adapter = HomeCatListAdapter(mContext!!, categories)

        rvBlog.layoutManager = LinearLayoutManager(mContext)
        rvBlog.adapter = HomeBlogListAdapter(mContext!!, blogs)

        rvTrendingProduct.layoutManager = GridLayoutManager(mContext, 3)
        rvTrendingProduct.adapter = RelatedProductListAdapter(tProducts, mContext!!,activity!!, "home")

        /*rvRelatedProduct.layoutManager =
                                    LinearLayoutManager(
                                        mContext!!,
                                        LinearLayoutManager.HORIZONTAL, false
                                    )
                                rvRelatedProduct.adapter = RelatedProductListAdapter(
                                    response.relatedProduct as ArrayList<ProductDatum>,
                                    mContext!!, this@ProductDetailActivity)*/

        if (tProducts.size>0){
            tvTrendingProduct.visibility = View.VISIBLE
        }else{
            tvTrendingProduct.visibility = View.GONE
        }
        if (categories.size>0){
            tvCategory.visibility = View.VISIBLE
        }else{
            tvCategory.visibility = View.GONE
        }
        if (blogs.size>0){
            tvBlog.visibility = View.VISIBLE
        }else{
            tvBlog.visibility = View.GONE
        }

    }

    private fun homeContent() {
        val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
        if (cd!!.isNetworkAvailable){
            RetrofitService.getHomeResponse(Dialog(mContext!!),
                retrofitApiClient!!.homeContent(userId), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response =  result!!.body() as HomeModel
                        if (response.status==200){
                            val banner: ArrayList<BannerDatum> = response.bannerData as ArrayList<BannerDatum>
                            val categories : ArrayList<Category> = response.category as ArrayList<Category>
                            val blogs : ArrayList<Blog> = response.blog as ArrayList<Blog>
                            val tProducts : ArrayList<ProductDatum> = response.trandingProduct as ArrayList<ProductDatum>
                            (activity!!.findViewById<TextView>(R.id.tvCartCount)).text = response.cartData.toString()
                            initSliderOne(banner)
                            initHomeCat(categories, blogs, tProducts)
                        }else{
                            mContext!!.showShortToast(response.message)
                        }
                    }
                })
        }
    }

    private fun initSliderOne(sliderOne: ArrayList<BannerDatum>) {
        val bannerListAdapter = SlidingImageAdapter(mContext, sliderOne)
        if (pager!=null) {
            pager.adapter = bannerListAdapter
            indicator.setViewPager(pager)
        }
        bannerListAdapter.notifyDataSetChanged()
        val density = resources.displayMetrics.density
        indicator.setRadius(3 * density)
        val NUM_PAGES = sliderOne.size
        //val NUM_PAGES = 2
        val handler = Handler()
        val update = Runnable {
            if (cp1 == NUM_PAGES) {
                cp1 = 0
            }
            if (pager!=null) {
                pager.setCurrentItem(cp1++, true)
            }
        }
        swipeTimer = Timer();
        swipeTimer!!.schedule(object : TimerTask(){
            override fun run() {
                handler.post(update)
            }
        }, 500, 3000)

        try {
            val mScroller: Field
            mScroller = ViewPager::class.java.getDeclaredField("mScroller")
            mScroller.isAccessible = true
            val scroller = FixedSpeedScroller(pager.getContext())
            //FixedSpeedScroller scroller = new FixedSpeedScroller(tokenPager.getContext(), new AccelerateInterpolator());
            // scroller.setFixedDuration(5000);
            mScroller[pager] = scroller
        } catch (ignored: NoSuchFieldException) {
        } catch (ignored: IllegalArgumentException) {
        } catch (ignored: IllegalAccessException) {
        }

        indicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(position: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                cp1 = position
            }

            override fun onPageSelected(position: Int) {

            }
        })
    }

    override fun clickedItem(subCatId: String?) {

    }

    override fun updateProductToCart(productId: String?, variationId: String?, quantity: Int) {

    }

    override fun deleteCartProduct(productId: String?, variationId: String?) {

    }

    override fun onClick(p0: View?) {
        /*when (p0!!.id) {
            R.id.rlVariations -> {
                val tag = p0.tag as Int
                if (isProductCat){
                    if (catProducts!![tag].productVariation.size>0) {
                        openVariationDialog(tag)
                    }
                }else{
                    if (subCatProducts!![tag].productVariation.size>0) {
                        openVariationDialog(tag)
                    }
                }
            }
        }*/
    }

    /*private fun openVariationDialog(position: Int) {
        val dialogBox = AlertDialog.Builder(mContext)
        dialogBox.setCancelable(false)

        val li = LayoutInflater.from(mContext)
        val view: View = li.inflate(R.layout.dialog_select_poduct_variation, null)
        dialogBox.setView(view)
        val alertDialog = dialogBox.create()
        alertDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        val rvProductType = view.findViewById<RecyclerView>(R.id.rvProductType)
        rvProductType.layoutManager = LinearLayoutManager(mContext!!)
        val variationAdapter : ProductVariationAdapter

        if (isProductCat){
            variationAdapter = ProductVariationAdapter(
                catProducts!![position].productVariation as ArrayList<ProductVariation>,
                mContext!!, View.OnClickListener {
                    val tag = it.tag as Int
                    for( i in catProducts!![position].productVariation.indices){
                        catProducts!![position].productVariation[i].isSelected = i==tag
                        alertDialog.dismiss()
                        productAdapter!!.notifyDataSetChanged()
                    }
                }, 0)
        }else{
            variationAdapter = ProductVariationAdapter(
                subCatProducts!![position].productVariation as ArrayList<ProductVariation>,
                mContext!!, View.OnClickListener {
                    val tag = it.tag as Int
                    for( i in subCatProducts!![position].productVariation.indices){
                        subCatProducts!![position].productVariation[i].isSelected = i==tag
                        alertDialog.dismiss()
                        productAdapter!!.notifyDataSetChanged()
                    }
                }, 0)
        }
        rvProductType.adapter = variationAdapter
        variationAdapter.notifyDataSetChanged()
        alertDialog.show()
    }*/
}