package com.shriagrohitech.ui.fragment

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.chatair_android.utils.showShortToast
import com.shriagrohitech.R
import com.shriagrohitech.adapter.OrderListAdapter
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.models.orders.MyOrdersModel
import com.shriagrohitech.models.orders.OrderDatum
import com.shriagrohitech.retrofit.RetrofitApiClient
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.fragment_my_matches.*
import retrofit2.Response

class MyOrdersFragment : Fragment() {

    var retrofitApiClient: RetrofitApiClient? = null
    var cd: ConnectionDetector? = null
    var mContext: Context? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_matches, container, false)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        mContext = activity
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()
        initOrders()
    }

    private fun initOrders() {
        val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
        if (cd!!.isNetworkAvailable){
            RetrofitService.orderResponse(
                Dialog(mContext!!), retrofitApiClient!!.getUserOrders(userId),
                object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as MyOrdersModel
                        if(response.status==200){
                            if (response.orderData.size>0) {
                                val orders = response.orderData as ArrayList<OrderDatum>
                                rvMyOrders.layoutManager = LinearLayoutManager(mContext)
                                rvMyOrders.adapter = OrderListAdapter(orders, mContext!!)
                            }
                        }
                    }
                })
        }
    }
}