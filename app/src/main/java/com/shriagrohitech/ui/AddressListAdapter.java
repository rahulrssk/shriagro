package com.shriagrohitech.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.shriagrohitech.R;
import com.shriagrohitech.models.address.UserAddress;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.MyViewHolder> {

    private ArrayList<UserAddress> addresses;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public AddressListAdapter(ArrayList<UserAddress> addresses, Context mContext,
                              View.OnClickListener onClickListener) {
        this.addresses = addresses;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_address_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final UserAddress productData = addresses.get(position);


        holder.tvAddressDetail.setText(productData.getAdd()+
                ","+productData.getCity()+","+productData.getState()+"-"+productData.getZipcode());
        holder.tvContact.setText("Contact : "+productData.getAddContact());

        holder.cvAddress.setTag(position);
        holder.cvAddress.setOnClickListener(onClickListener);

        holder.buttonViewOption.setTag(position);
        holder.buttonViewOption.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return addresses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvAddressName, tvAddressDetail,tvContact,tvCity,tvStateZip;
        ImageView buttonViewOption;
        private CardView cvAddress;


        public MyViewHolder(View view) {
            super(view);
            tvAddressDetail = view.findViewById(R.id.tvAddressDetail);
            tvContact = view.findViewById(R.id.tvContact);
            cvAddress = view.findViewById(R.id.cvAddress);
            buttonViewOption  = view.findViewById(R.id.ivMore);

        }
    }
}


