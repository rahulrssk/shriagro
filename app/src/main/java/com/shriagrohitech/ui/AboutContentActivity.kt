package com.shriagrohitech.ui

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.text.Html
import com.chatair_android.utils.showShortToast
import com.shriagrohitech.R
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.activity_about_content.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

class AboutContentActivity : BaseActivity() {
    private var from = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_content)

        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        from = intent.extras!!.getString("from")!!
        if (from == "tc"){
            tvTitle.text = resources.getString(R.string.terms_condition)
        }else if (from == "about"){
            tvTitle.text = resources.getString(R.string.about_us)
        }else if (from == "privacy"){
            tvTitle.text = resources.getString(R.string.privacy_policy)
        }
        ivBack.setOnClickListener { onBackPressed() }
        fetchContent()
    }

    private fun fetchContent() {
        if (cd!!.isNetworkAvailable){
            RetrofitService.getServerResponse(
                Dialog(mContext!!), retrofitApiClient!!.content(), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as ResponseBody
                        val responseObject = JSONObject(response.string())
                        val status = responseObject.getInt("status")
                        val msg = responseObject.getString("message")
                        val content: JSONObject = responseObject.getJSONArray("content")[0] as JSONObject
                        val abt = content.getString("about_us")
                        val tc = content.getString("term_and_conditions")
                        val privacy = content.getString("privacy_policy")
                        if (status == 200) {
                            ///tvContent
                            if (from == "tc"){
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvContent.text = Html.fromHtml(tc, Html.FROM_HTML_MODE_LEGACY);
                                } else {
                                    tvContent.text = Html.fromHtml(tc);
                                }
                            }else if (from == "about"){
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvContent.text = Html.fromHtml(abt, Html.FROM_HTML_MODE_LEGACY);
                                } else {
                                    tvContent.text = Html.fromHtml(abt);
                                }
                            }else if (from == "privacy"){
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvContent.text = Html.fromHtml(privacy, Html.FROM_HTML_MODE_LEGACY);
                                } else {
                                    tvContent.text = Html.fromHtml(privacy);
                                }
                            }
                        }else {
                            mContext!!.showShortToast(msg)
                        }
                    }
                })
        }
    }

    /*{
    "status": 200,
    "message": "data exist",
    "content": [
        {
            "about_us": "<p>About Us content will come here&nbsp;</p>\r\n",
            "term_and_conditions": "<p>Term &amp;&nbsp; Condition Content Will Come Here</p>\r\n",
            "privacy_policy": "<p>Privacy Policy Content Will Come Here</p>\r\n",
            "faq": "<p>Your Faq Will Come Here</p>\r\n",
            "contact_detail": "saj"
        }
    ]
}*/
}