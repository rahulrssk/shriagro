package com.shriagrohitech.ui

import android.app.Dialog
import android.os.Bundle
import com.chatair_android.utils.showShortToast
import com.shriagrohitech.R
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.models.address.UserAddress
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.activity_add_address.*
import kotlinx.android.synthetic.main.activity_add_address.ivBack
import kotlinx.android.synthetic.main.activity_cart.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

class AddAddressActivity : BaseActivity() {
    private var addressType = "Home"
    private var from = "add"
    private val fromAdd = "add"
    private val fromUpdate = "update"
    private lateinit var address: UserAddress
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_address)

        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        from = intent.extras!!.getString("from")!!

        if (from==fromAdd){
            tvTitle.text = "Add Address"
        }else if (from==fromUpdate){
            tvTitle.text = "Update Address"
            address = intent.getParcelableExtra("address")!!
        }

        ivBack.setOnClickListener { onBackPressed() }
        initViews()
    }

    private fun initViews() {

        if (from==fromUpdate){
            etAddress.setText(address.add)
            etCity.setText(address.city)
            etState.setText(address.state)
            etZipcode.setText(address.zipcode)
            etMobile.setText(address.addContact)
            etAlternetContact.setText(address.addAlternetContact)
            etLandMark.setText(address.landmark)
            btnAddAddress.text = "Update"
        }

        btnAddAddress.setOnClickListener {
            val add1 = etAddress.text.toString().trim()
            val city = etCity.text.toString().trim()
            val state = etState.text.toString().trim()
            val zip = etZipcode.text.toString().trim()
            val contact = etMobile.text.toString().trim()
            val aContact = etAlternetContact.text.toString().trim()
            val landmark = etLandMark.text.toString().trim()
            if (add1.isEmpty()){
                mContext!!.showShortToast("Address should not be empty")
            }else if (city.isEmpty()){
                mContext!!.showShortToast("City should not be empty")
            }else if (state.isEmpty()){
                mContext!!.showShortToast("State should not be empty")
            }else if (zip.isEmpty()||zip.length<6){
                mContext!!.showShortToast("Enter a valid Zipcode")
            }else if (contact.isEmpty()||contact.length<10){
                mContext!!.showShortToast("Enter a valid Mobile Number")
            }else{
                if (from==fromAdd) {
                    addUserAddress("0", add1, city, state, zip, contact, aContact, landmark)
                } else if (from==fromUpdate){
                    addUserAddress(address.addressId, add1, city, state, zip, contact, aContact, landmark)
                }
            }
        }

    }

    private fun addUserAddress(
        addID: String,
        add1: String,
        city: String,
        state: String,
        zip: String,
        contact: String,
        aContact: String,
        landmark: String
    ) {
        val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
        if (cd!!.isNetworkAvailable){
            RetrofitService.getServerResponse(
                Dialog(mContext!!), retrofitApiClient!!.addUserAddress(
                userId, addID, add1, city, state, zip, addressType, contact, aContact, landmark),
                object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as ResponseBody
                        val responseObject = JSONObject(response.string())
                        val status = responseObject.getInt("status")
                        val msg = responseObject.getString("message")
                        if (status == 200) {
                            onBackPressed()
                        }
                        mContext!!.showShortToast(msg)
                    }
                })
        }
    }
}