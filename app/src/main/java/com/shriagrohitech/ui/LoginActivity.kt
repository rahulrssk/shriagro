package com.shriagrohitech.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import com.shriagrohitech.R
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

class LoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        fabNext.setOnClickListener {
            doLogin()
            /*startActivity(Intent(mContext!!, FirebaseOtpActivity::class.java)
                .putExtra("mobileNumber", "1234567890")
                .putExtra("userType", "new"))*/
            //finish()
        }
    }

    private fun doLogin(){
        val mobileNo = etMobile.text.toString().trim()
        cbTnC
        if (mobileNo.isEmpty() || mobileNo.length<10){
            showToast(mContext!!, "Please enter a valid Mobile No")
        }else if (!cbTnC.isChecked){
            showToast(mContext!!, "Check the box to proceed")
        }else{
            if (cd!!.isNetworkAvailable){
                RetrofitService.getServerResponse(Dialog(mContext!!),
                    retrofitApiClient!!.requestOtpLogin(mobileNo), object : WebResponse {
                        override fun onResponseFailed(error: String?) {
                                showToast(mContext!!, "Server Error...")
                        }

                        override fun onResponseSuccess(result: Response<*>?) {
                           val response = result!!.body() as ResponseBody
                            val responseObject = JSONObject(response.string())
                            val status = responseObject.getInt("status")
                            val msg = responseObject.getString("message")
                            val otp = responseObject.getString("otp")
                            val mobileNumber = responseObject.getString("mobile")
                            val userType = responseObject.getString("user_type")
                            if (status==200){
                                startActivity(Intent(mContext!!, FirebaseOtpActivity::class.java)
                                    .putExtra("mobileNumber", mobileNumber)
                                    .putExtra("userType", userType)
                                    .putExtra("serverOtp", otp))
                                finish()
                            }
                            showToast(mContext!!, msg)
                        }
                    })
            }
        }
    }

}

/*{
    "status": 200,
    "message": "SuccessFully Created Account",
    "mobile_number": "8528528529",
    "user_id": true,
    "user_type": "New"
}
{
    "status": 200,
    "message": "Otp Sent On Your Mobile Number",
    "mobile_number": "9589306178",
    "user_type": "Exist"
}*/