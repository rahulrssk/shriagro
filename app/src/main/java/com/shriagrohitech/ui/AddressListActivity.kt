package com.shriagrohitech.ui

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import com.chatair_android.utils.showShortToast
import com.google.firebase.auth.FirebaseAuth
import com.shriagrohitech.R
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.models.address.AddressModel
import com.shriagrohitech.models.address.UserAddress
import com.shriagrohitech.retrofit.RetrofitService
import com.shriagrohitech.retrofit.WebResponse
import com.shriagrohitech.utils.AppPreference
import com.shriagrohitech.utils.BaseActivity
import com.shriagrohitech.utils.ConnectionDetector
import kotlinx.android.synthetic.main.activity_address_list.*
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_cart.ivBack
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Response

class AddressListActivity : BaseActivity(), View.OnClickListener  {
    private var addresses: ArrayList<UserAddress> = ArrayList()
    private var addressAdapter: AddressListAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_address_list)

        mContext = this
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        ivBack.setOnClickListener { onBackPressed() }
        ivAddAddress.setOnClickListener {
            startActivity(Intent(mContext!!, AddAddressActivity::class.java)
                .putExtra("from", "add"))
        }
    }

    override fun onStart() {
        super.onStart()
        fetchAddress()
    }

    private fun fetchAddress() {
        if (cd!!.isNetworkAvailable){
            val userId = AppPreference.getStringPreference(mContext!!, Constant.USER_ID)
            RetrofitService.userAddressList(
                Dialog(mContext!!),
                retrofitApiClient!!.userAddress(userId), object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response =  result!!.body() as AddressModel
                        if (response.status==200){
                            addresses = response.userAddress as ArrayList<UserAddress>
                            addressAdapter = AddressListAdapter(addresses, mContext!!, this@AddressListActivity)
                            rvAddress.layoutManager = LinearLayoutManager(mContext!!)
                            rvAddress.adapter = addressAdapter
                            addressAdapter!!.notifyDataSetChanged()
                        }
                    }
                })
        }
    }

    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.cvAddress -> {
                val tag: Int = p0.tag as Int
                val id = addresses[tag].addressId
                val cIntent = Intent(mContext, CheckoutActivity::class.java)
                cIntent.putExtra("addressId", addresses[tag])
                setResult(312, cIntent)
                finish()
            }
            R.id.ivMore -> {
                val openMenuTag: Int = p0.tag as Int
                openPopupDialog(addresses[openMenuTag], openMenuTag)
            }
        }
    }

    private fun openPopupDialog(
        address: UserAddress,
        openMenuTag: Int
    ) {
        val popup = PopupMenu(
            mContext!!,
            rvAddress.getChildAt(openMenuTag).findViewById(R.id.ivMore)
        )
        //inflating menu from xml resource
        popup.inflate(R.menu.menu_popup_address)
        popup.setOnMenuItemClickListener { item ->
            when (item!!.itemId) {
                R.id.menuUpdateAddress -> {
                    startActivity(
                        Intent(mContext, AddAddressActivity::class.java)
                            .putExtra("from", "update")
                            .putExtra("address", address)
                    )

                }
                R.id.menuDeleteAddress -> {
                    val dialog = AlertDialog.Builder(mContext!!, R.style.AlertDialogTheme)
                    dialog
                        .setTitle("Delete Address")
                        .setMessage("Are you sure want to delete this address ?")
                        .setPositiveButton(
                            "YES"
                        ) { _: DialogInterface?, _: Int ->
                            deleteAddress(address.addressId, openMenuTag)
                        }
                        .setNegativeButton("NO", null)
                        .create()
                        .show()
                }
            }
            false
        }
        popup.show()

    }

    private fun deleteAddress(addressId: String?, openMenuTag: Int) {
        if (cd!!.isNetworkAvailable){
            RetrofitService.getServerResponse(
                Dialog(mContext!!),
                retrofitApiClient!!.deleteAddress(addressId),
                object : WebResponse {
                    override fun onResponseFailed(error: String?) {
                        mContext!!.showShortToast("Server Error...")
                    }

                    override fun onResponseSuccess(result: Response<*>?) {
                        val response = result!!.body() as ResponseBody
                        val responseObject = JSONObject(response.string())
                        val status = responseObject.getInt("status")
                        val msg = responseObject.getString("message")
                        if (status == 200) {
                            addresses.removeAt(openMenuTag)
                            addressAdapter!!.notifyDataSetChanged()
                        }
                        mContext!!.showShortToast(msg)
                    }
                })
        }
    }
}