package com.infobitetechnology.samon.interfaces

interface CartListener {
    fun updateProductToCart(
        productId: String?,
        variationId: String?,
        quantity: Int
    )
    fun deleteCartProduct(
        productId: String?,
        variationId: String?
    )
}