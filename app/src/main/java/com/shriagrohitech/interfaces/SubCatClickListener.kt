package com.infobitetechnology.samon.interfaces

interface SubCatClickListener {
    fun clickedItem(subCatId: String?)
}