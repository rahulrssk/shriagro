package com.shriagrohitech.constant;

public class Constant {

    public static final String USER_DATA = "userData";
    public static final String IS_COMPLETE_ADDRESS = "isCompleteAddress";
    public static final String IS_LOGIN = "isLogin";
    public static final String USER_ID = "userID";
    public static final String slash = "/";

}