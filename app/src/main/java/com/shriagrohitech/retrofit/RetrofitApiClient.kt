package com.shriagrohitech.retrofit

import com.shriagrohitech.models.address.AddressModel
import com.shriagrohitech.models.cart.CartModel
import com.shriagrohitech.models.cat_data.CategoryDataModel
import com.shriagrohitech.models.cat_data.ProductDetailModel
import com.shriagrohitech.models.cat_data.SubCatDataModel
import com.shriagrohitech.models.home.HomeModel
import com.shriagrohitech.models.offers.OffersModel
import com.shriagrohitech.models.orders.MyOrdersModel
import com.shriagrohitech.models.slot.TimeSlotlModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface RetrofitApiClient {

    @FormUrlEncoded
    @POST("User_api/user_login_signup")
    fun requestOtpLogin(@Field("mobile") mobile: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("User_api/check_validation_code")
    fun verifyOTP(
        @Field("mobile") mobile: String?,
        @Field("otp") otp: String?
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("User_api/home_page")
    fun homeContent(@Field("user_id") user_id: String?): Call<HomeModel?>?

    @Multipart
    @POST("User_api/update_user_image")
    fun updateUserProfile(
        @Part("user_id") user_id: RequestBody?,
        @Part image: MultipartBody.Part?
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("User_api/update_user_profile")
    fun updateUser(
        @Field("user_id") user_id: String?,
        @Field("name") name: String?,
        @Field("gender") gender: String?,
        @Field("email") email: String?,
        @Field("gaon") gaon: String?,
        @Field("tehsil") tehsil: String?,
        @Field("city") city: String?,
        @Field("pincode") pincode: String?
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("User_api/get_all_product")
    fun catData(
        @Field("main_cat_id") main_cat_id: String?,
        @Field("user_id") user_id: String?
    ): Call<CategoryDataModel?>?

    @FormUrlEncoded
    @POST("User_api/get_subcat_product")
    fun subCatData(
        @Field("sub_cat_id") sub_cat_id: String?,
        @Field("user_id") user_id: String?
    ): Call<SubCatDataModel?>?

    @FormUrlEncoded
    @POST("User_api/add_update_cart")
    fun addUpdateCart(
        @Field("user_cart_id") user_cart_id: String?,
        @Field("user_id") user_id: String?,
        @Field("product_id") product_id: String?,
        @Field("quantity") quantity: String?,
        @Field("variation_id") variation_id: String?,
        @Field("variation_market_price") variation_market_price: String?,
        @Field("variation_selling_price") variation_selling_price: String?,
        @Field("variation_name") variation_name: String?,
        @Field("variation_quantity") variation_quantity: String?
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("User_api/delete_user_cart")
    fun deleteCartProduct(@Field("user_cart_id") user_cart_id: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("User_api/get_user_cart")
    fun cartData(@Field("user_id") user_id: String?): Call<CartModel?>?

    @FormUrlEncoded
    @POST("User_api/get_single_product")
    fun singleProductDetail(
        @Field("product_id") product_id: String?,
        @Field("user_id") user_id: String?
    ): Call<ProductDetailModel?>?

    @FormUrlEncoded
    @POST("User_api/user_order")
    fun userOrder(
        @Field("user_id") user_id: String?,
        @Field("promo_code") promo_code: String?,
        @Field("payment_method") payment_method: String?,
        @Field("transaction_id") transaction_id: String?,
        @Field("total_amount") total_amount: String?,
        @Field("delivery_schedule") delivery_schedule: String?,
        @Field("delivery_charges") delivery_charges: String?,
        @Field("address_id") address_id: String?,
        @Field("order_create_date") order_create_date: String?,
        @Field("payment_status") payment_status: String?
    ): Call<ResponseBody?>?

    @GET("User_api/get_time_slot")
    fun timeSlot(): Call<TimeSlotlModel?>?

    @FormUrlEncoded
    @POST("User_api/get_user_address")
    fun userAddress(@Field("user_id") user_id: String?): Call<AddressModel?>?

    @FormUrlEncoded
    @POST("User_api/delete_address")
    fun deleteAddress(@Field("address_id") address_id: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("User_api/add_update_address")
    fun addUserAddress(
        @Field("user_id") user_id: String?,
        @Field("address_id") address_id: String?,
        @Field("add") add: String?,
        @Field("city") city: String?,
        @Field("state") state: String?,
        @Field("zipcode") zipcode: String?,
        @Field("add_type") add_type: String?,
        @Field("add_contact") add_contact: String?,
        @Field("add_alternet_contact") add_alternet_contact: String?,
        @Field("landmark") landmark: String?
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("User_api/get_user_order")
    fun getUserOrders(@Field("user_id") user_id: String?): Call<MyOrdersModel?>?

    @FormUrlEncoded
    @POST("User_api/get_single_blog")
    fun getBlogDetails(@Field("blog_id") blog_id: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("User_api/check_promotion_list")
    fun checkCoupon(@Field("user_id") user_id: String?,
                    @Field("promo_code") promo_code: String?): Call<ResponseBody?>?

    @GET("User_api/app_content")
    fun content(): Call<ResponseBody?>?

    @GET("User_api/get_promotion_list")
    fun offers(): Call<OffersModel?>?
}