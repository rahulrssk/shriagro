package com.shriagrohitech.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shriagrohitech.R
import com.shriagrohitech.models.orders.OrderDatum


class OrderListAdapter(private val nameList: ArrayList<OrderDatum>, private val mContext: Context) :
    RecyclerView.Adapter<OrderListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_user_order, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data :OrderDatum = nameList[position]
        holder.tvOrderId.text = "Order No ${data.orderId}"
        holder.tvItemCount.text = "${data.productData.size} Item(s)"
        var itemName = ""
        data.productData.forEach {
            if (itemName.isEmpty()){
                itemName = it.productName
            }else{
                itemName = "$itemName, ${it.productName}"
            }
        }
        holder.tvItems.text = itemName

        holder.tvOrderDate.text = "Order Date : ${data.orderCreateDate}"
        holder.tvOrderSchedule.text = "Schedule At : ${data.deliverySchedule}"
        holder.tvOrderAmount.text = "Order Amount : ₹${data.totalAmount}"
        holder.tvOrderMode.text = "Payment Mode : ${data.paymentMethod}"
        holder.tvOrderStatus.text = data.finalOrderStatus
        holder.rlRoot.setOnClickListener {

        }
    }

    override fun getItemCount(): Int {
        return nameList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvOrderDate = itemView.findViewById(R.id.tvOrderDate) as TextView
        val tvOrderId = itemView.findViewById(R.id.tvOrderId) as TextView
        val tvItemCount = itemView.findViewById(R.id.tvItemCount) as TextView
        val tvItems = itemView.findViewById(R.id.tvItems) as TextView
        val tvOrderStatus = itemView.findViewById(R.id.tvOrderStatus) as TextView
        val tvOrderSchedule = itemView.findViewById(R.id.tvOrderSchedule) as TextView
        val tvOrderAmount = itemView.findViewById(R.id.tvOrderAmount) as TextView
        val tvOrderMode = itemView.findViewById(R.id.tvOrderMode) as TextView
        val rlRoot = itemView.findViewById(R.id.rlRoot) as RelativeLayout
    }
}