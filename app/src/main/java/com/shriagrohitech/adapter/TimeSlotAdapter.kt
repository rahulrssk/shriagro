package com.shriagrohitech.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.shriagrohitech.R
import com.shriagrohitech.models.slot.VendorTimeSlot


class TimeSlotAdapter(private val nameList: ArrayList<VendorTimeSlot>,
                      private val mContext: Context,
                      private val clickListener: View.OnClickListener) :
    RecyclerView.Adapter<TimeSlotAdapter.ViewHolder>() {

    private var lastChecked: RadioButton? = null
    private var lastCheckedPos = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_time_slot, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.rbChecked.text = nameList[position].timeSlot
        holder.rbChecked.isChecked = nameList[position].isSelected

        holder.rbChecked.tag = position
        holder.rbChecked.setOnClickListener (clickListener)

    }

    override fun getItemCount(): Int {
        return nameList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rbChecked = itemView.findViewById(R.id.rbChecked) as RadioButton
    }

    fun selectedSlot():ArrayList<VendorTimeSlot>{
        return nameList
    }
}