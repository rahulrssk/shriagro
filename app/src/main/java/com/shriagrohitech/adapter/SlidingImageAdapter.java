package com.shriagrohitech.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.PagerAdapter;


import com.bumptech.glide.Glide;
import com.shriagrohitech.R;
import com.shriagrohitech.models.home.BannerDatum;

import java.util.ArrayList;

public class SlidingImageAdapter extends PagerAdapter {

    private ArrayList<BannerDatum> bannerList;
    private LayoutInflater inflater;
    private Context context;

    public SlidingImageAdapter(Context context, ArrayList<BannerDatum> bannerList) {
        this.context = context;
        this.bannerList = bannerList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return bannerList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = inflater.inflate(R.layout.row_banner, container, false);
        ImageView imageView = itemView.findViewById(R.id.imageView_banner);
        //imageView.setImageDrawable(context.getResources().getDrawable(bannerList.get(position).getImage()));
        Glide.with(context)
                    .load(bannerList.get(position).getBannerImage())
                    .fitCenter()
                    .placeholder(R.drawable.placeholder_banner)
                    .error(R.drawable.placeholder_banner)
                    .into(imageView);
            container.addView(itemView);

        /*if (position==0) {
            Glide.with(context)
                    .load(R.drawable.slider2)
                    .fitCenter()
                    .placeholder(R.drawable.placeholder_banner)
                    .error(R.drawable.placeholder_banner)
                    .into(imageView);
            container.addView(itemView);
        }else{
            Glide.with(context)
                    .load(R.drawable.slider1)
                    .fitCenter()
                    .placeholder(R.drawable.placeholder_banner)
                    .error(R.drawable.placeholder_banner)
                    .into(imageView);
            container.addView(itemView);
        }*/
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
