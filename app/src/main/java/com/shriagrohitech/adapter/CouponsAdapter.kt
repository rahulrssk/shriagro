package com.shriagrohitech.adapter

import android.content.ClipData
import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.text.ClipboardManager
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shriagrohitech.R
import com.shriagrohitech.models.offers.PromotionList
import java.util.*

class CouponsAdapter(private val catList: ArrayList<PromotionList>, private val mContext: Context) :
    RecyclerView.Adapter<CouponsAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_coupons, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvTitle.text = "USE COUPON : " + catList[position].promoCode
        Glide.with(mContext)
            .load(catList[position].promoImage)
            .centerCrop()
            .placeholder(R.drawable.gallery)
            .error(R.drawable.gallery)
            .into(holder.ivCoupon)
        holder.ivCopy.setOnClickListener { v: View? ->
            if (copyToClipboard(
                    mContext, catList[position].promoCode
                )
            ) {
                Toast.makeText(
                    mContext, "Coupon code copied to clipboard.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    mContext, "Some Error Occurred.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun getItemCount(): Int {
        return catList.size
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvTitle: TextView
        var ivCoupon: ImageView
        var ivCopy: ImageView
        var llRoot: RelativeLayout

        init {
            tvTitle = view.findViewById(R.id.tvTitle)
            ivCoupon = view.findViewById(R.id.ivCoupon)
            ivCopy = view.findViewById(R.id.ivCopy)
            llRoot = view.findViewById(R.id.llRoot)
        }
    }

    private fun copyToClipboard(context: Context, text: String?): Boolean {
        return try {
            val sdk = Build.VERSION.SDK_INT
            if (sdk < Build.VERSION_CODES.HONEYCOMB) {
                val clipboard = context
                    .getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                clipboard.text = text
            } else {
                val clipboard = context
                    .getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
                val clip = ClipData
                    .newPlainText("Copied to clipboard", text)
                clipboard.setPrimaryClip(clip)
            }
            true
        } catch (e: Exception) {
            false
        }
    }

    private fun dpToPx(dp: Int): Int {
        val r: Resources = mContext.getResources()
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            r.displayMetrics
        ).toInt()
    }
}