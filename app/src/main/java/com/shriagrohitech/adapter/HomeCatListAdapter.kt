package com.shriagrohitech.adapter

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shriagrohitech.R
import com.shriagrohitech.models.home.Category
import com.shriagrohitech.ui.CatProductActivity


/*class HomeCatListAdapter(private val nameList: ArrayList<String>, private val mContext: Context,
                         private val clickListener: View.OnClickListener) :*/
class HomeCatListAdapter(
    private val mContext: Context,
    private val categories: ArrayList<Category>
) :
    RecyclerView.Adapter<HomeCatListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_home_cat_grid, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val category = categories[position]
        holder.tvCatTitle.text = category.mainCatName
        Glide.with(mContext)
            .load(category.mainCatImage)
            .placeholder(R.drawable.icf_leaves)
            .error(R.drawable.icf_leaves)
            .into(holder.ivCat)
        holder.rlImage.setOnClickListener {
            mContext.startActivity(Intent(mContext, CatProductActivity::class.java)
                .putExtra("data", category))
        }
    }

    override fun getItemCount(): Int {
        return categories.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvCatTitle = itemView.findViewById(R.id.tvCatTitle) as TextView
        val ivCat = itemView.findViewById(R.id.ivCat) as ImageView
        val rlImage = itemView.findViewById(R.id.rlImage) as RelativeLayout
        val rlRoot = itemView.findViewById(R.id.rlRoot) as RelativeLayout
    }

    private fun dpToPx(dp: Int): Int {
        val r: Resources = mContext.getResources()
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            r.displayMetrics
        ).toInt()
    }
}