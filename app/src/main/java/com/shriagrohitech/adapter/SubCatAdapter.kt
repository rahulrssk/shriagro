package com.shriagrohitech.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.infobitetechnology.samon.interfaces.SubCatClickListener
import com.shriagrohitech.R
import com.shriagrohitech.models.cat_data.SubCategory


class SubCatAdapter(private val nameList: ArrayList<SubCategory>, private val mContext: Context,
                    private val subCAtListener: SubCatClickListener) :
    RecyclerView.Adapter<SubCatAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_sub_cat, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data :SubCategory = nameList[position]

        holder.tvSubCatName.text = data.subCatName

        if (data.isSelected){
            holder.tvSubCatName.setTextColor(Color.parseColor("#FFFFFF"))
            holder.llSubCat.background = mContext.getDrawable(R.drawable.bg_sub_cat_solid)
        }else{
            holder.tvSubCatName.setTextColor(Color.parseColor("#2DAA69"))
            holder.llSubCat.background = mContext.getDrawable(R.drawable.bg_sub_cat_outline)
        }

        holder.llSubCat.setOnClickListener{
            subCAtListener.clickedItem(data.subCatId)
        }
    }

    override fun getItemCount(): Int {
        return nameList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvSubCatName = itemView.findViewById(R.id.tvSubCatName) as TextView
        val llSubCat = itemView.findViewById(R.id.llSubCat) as LinearLayout
    }
}