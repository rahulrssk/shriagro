package com.shriagrohitech.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.infobitetechnology.samon.interfaces.CartListener
import com.shriagrohitech.R
import com.shriagrohitech.models.cat_data.ProductDatum
import com.shriagrohitech.ui.ProductDetailActivity


class TrendingProductAdapter(
    private val nameList: ArrayList<ProductDatum>, private val mContext: Context,
    private val cartListener: CartListener, private val clickListener: View.OnClickListener
) :
    RecyclerView.Adapter<TrendingProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_product_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data: ProductDatum = nameList[position]

        holder.tvProductName.text = data.productName
        Glide.with(mContext)
            .load(data.productImage)
            .centerCrop()
            .placeholder(R.drawable.gallery)
            .error(R.drawable.gallery)
            .into(holder.ivProduct)
        if (data.productVariation.size > 0) {
            var selected = false
            data.productVariation.forEach {
                if (it.isSelected) {
                    selected = true
                    holder.tvVariations.text = "${it.variationQuantity} " +
                            "${it.variation}"
                    holder.tvPrice.text = "Price : ₹${it.stockPrice}"
                    var stockCount = 0
                    try {
                        stockCount = (it.stock).toInt()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    if (stockCount>0) {
                        holder.tvStock.text = "In Stock"
                        holder.tvStock.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                    } else {
                        holder.tvStock.text = "Out of Stock"
                        holder.tvStock.setTextColor(ContextCompat.getColor(mContext, R.color.colorPr1))
                    }

                    holder.tvMrp.text = "MRP : ₹${it.stockMrp}"
                    holder.tvMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                    if (it.cartData > 0) {
                        holder.rlCartQuantity.visibility = View.VISIBLE
                        holder.tvAddToCart.visibility = View.GONE
                        holder.tvCartCount.text = "${it.cartQuantity}"
                    } else {
                        holder.rlCartQuantity.visibility = View.GONE
                        holder.tvAddToCart.visibility = View.VISIBLE
                    }
                }
            }
            if (!selected) {
                holder.tvVariations.text = "${data.productVariation[0].variationQuantity} " +
                        "${data.productVariation[0].variation}"
                holder.tvPrice.text = "${data.productVariation[0].variationQuantity} " +
                        "${data.productVariation[0].variation}"
                holder.tvPrice.text = "Price : ₹${data.productVariation[0].stockPrice}"

                var stockCount = 0
                try {
                    stockCount = (data.productVariation[0].stock).toInt()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                if (stockCount>0) {
                    holder.tvStock.text = "In Stock"
                    holder.tvStock.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                } else {
                    holder.tvStock.text = "Out of Stock"
                    holder.tvStock.setTextColor(ContextCompat.getColor(mContext, R.color.colorPr1))
                }

                holder.tvMrp.text = "MRP : ₹${data.productVariation[0].stockMrp}"
                holder.tvMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                /*holder.tvProductMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);*/
                if (data.productVariation[0].cartData > 0) {
                    holder.rlCartQuantity.visibility = View.VISIBLE
                    holder.tvAddToCart.visibility = View.GONE
                    holder.tvCartCount.text = "${data.productVariation[0].cartQuantity}"
                } else {
                    holder.rlCartQuantity.visibility = View.GONE
                    holder.tvAddToCart.visibility = View.VISIBLE
                }
            }
        }
        holder.rlVariations.setTag(position)
        holder.rlVariations.setOnClickListener (clickListener)
        holder.tvAddToCart.setOnClickListener {
            if (data.productVariation.size > 0) {
                var isVariationSelected = false
                data.productVariation.forEach {
                    if (it.isSelected){
                        isVariationSelected = true
                        cartListener.updateProductToCart(it.productId, it.variationId, 1)
                    }
                }
                if (!isVariationSelected){
                    cartListener.updateProductToCart(data.productVariation[0].productId,
                        data.productVariation[0].variationId, 1)
                }
            }
        }
        holder.ivPlusCart.setOnClickListener {
            if (data.productVariation.size > 0) {
                var isVariationSelected = false
                data.productVariation.forEach {
                    if (it.isSelected){
                        isVariationSelected = true
                        val qty:Int = (it.cartQuantity.toInt())+1
                        cartListener.updateProductToCart(it.productId, it.variationId, qty)
                    }
                }
                if (!isVariationSelected){
                    val qty = (data.productVariation[0].cartQuantity.toInt())+1
                    cartListener.updateProductToCart(data.productVariation[0].productId,
                        data.productVariation[0].variationId, qty)
                }
            }
        }
        holder.ivMinusCart.setOnClickListener {
            if (data.productVariation.size > 0) {
                var isVariationSelected = false
                data.productVariation.forEach {
                    if (it.isSelected){
                        isVariationSelected = true
                        val qt = (it.cartQuantity.toInt())
                        var qty = 0
                        if (qt>0) {
                            qty = qt - 1
                        }
                        if (qty>0){
                            cartListener.updateProductToCart(it.productId, it.variationId, qty)
                        }else{
                            cartListener.deleteCartProduct(it.productId, it.variationId)
                        }
                    }
                }
                if (!isVariationSelected){
                    val qt = ( data.productVariation[0].cartQuantity.toInt())
                    var qty = 0
                    if ( qt>0) {
                        qty = qt - 1
                    }
                    if (qty>0){
                        cartListener.updateProductToCart(data.productVariation[0].productId,
                            data.productVariation[0].variationId, qty)
                    }else{
                        cartListener.deleteCartProduct(data.productVariation[0].productId,
                            data.productVariation[0].variationId)
                    }
                }
            }
        }

        holder.rlProduct.setOnClickListener {
            mContext.startActivity(Intent(mContext, ProductDetailActivity::class.java)
                .putExtra("productId", data.productId)
                .putExtra("productName", data.productName))
        }

    }

    override fun getItemCount(): Int {
        return nameList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvProductName = itemView.findViewById(R.id.tvProductName) as TextView
        val tvVariations = itemView.findViewById(R.id.tvVariations) as TextView
        val tvPrice = itemView.findViewById(R.id.tvPrice) as TextView
        val tvMrp = itemView.findViewById(R.id.tvMrp) as TextView
        val tvStock = itemView.findViewById(R.id.tvStock) as TextView
        val tvAddToCart = itemView.findViewById(R.id.tvAddToCart) as TextView
        val tvCartCount = itemView.findViewById(R.id.tvCartCount) as TextView
        val ivProduct = itemView.findViewById(R.id.ivProduct) as ImageView
        val ivPlusCart = itemView.findViewById(R.id.ivPlusCart) as ImageView
        val ivMinusCart = itemView.findViewById(R.id.ivMinusCart) as ImageView
        val rlVariations = itemView.findViewById(R.id.rlVariations) as RelativeLayout
        val rlProduct = itemView.findViewById(R.id.rlProduct) as RelativeLayout
        val rlCArtAction = itemView.findViewById(R.id.rlCArtAction) as RelativeLayout
        val rlCartQuantity = itemView.findViewById(R.id.rlCartQuantity) as RelativeLayout
    }
}