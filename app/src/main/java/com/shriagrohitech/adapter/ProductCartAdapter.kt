package com.shriagrohitech.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.infobitetechnology.samon.interfaces.CartListener
import com.shriagrohitech.R
import com.shriagrohitech.models.cart.UserCartDatum
import com.shriagrohitech.ui.ProductDetailActivity


class ProductCartAdapter(
    private val nameList: ArrayList<UserCartDatum>, private val mContext: Context,
    private val cartListener: CartListener
) :
    RecyclerView.Adapter<ProductCartAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_product_list_card, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        //LTRB
        if (position == 0) {
            params.setMargins(dpToPx(12), dpToPx(12), dpToPx(12), dpToPx(6))
        } else if(position==((nameList.size)-1)) {
            params.setMargins(dpToPx(12), dpToPx(6), dpToPx(12), dpToPx(12))
        } else{
            params.setMargins(dpToPx(12), dpToPx(6), dpToPx(12), dpToPx(6))
        }

        holder.rlProduct.setLayoutParams(params)

        val data: UserCartDatum = nameList[position]

        holder.tvStock.visibility = View.GONE
        holder.tvProductName.text = data.productName
        Glide.with(mContext)
            .load(data.productImage)
            .centerCrop()
            .into(holder.ivProduct)

        holder.tvVariations.text = "${data.variationQuantity} " +
                "${data.variationName}"

        /*"%.2f".format(amt)*/
        var vsPrice = 0.0
        var marketPrice = 0.0

        try {
            vsPrice = (data.variationSellingPrice).toDouble()
            marketPrice = (data.variationMarketPrice).toDouble()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val variationSellingPrice = "%.2f".format(vsPrice)
        val variationMarketPrice = "%.2f".format(marketPrice)

        holder.tvPrice.text = "Price : ₹$variationSellingPrice"
        holder.tvMrp.text = "MRP : ₹$variationMarketPrice"
        holder.tvMrp.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        holder.ivArrow.visibility = View.GONE

        holder.rlCartQuantity.visibility = View.VISIBLE
        holder.tvAddToCart.visibility = View.GONE
        holder.tvCartCount.text = "${data.quantity}"


        holder.ivPlusCart.setOnClickListener {

            val qty = (data.quantity.toInt()) + 1
            cartListener.updateProductToCart(
                data.productId,
                data.variationId, qty
            )

        }
        holder.ivMinusCart.setOnClickListener {

            val qt = (data.quantity.toInt())
            var qty = 0
            if (qt > 0) {
                qty = qt - 1
            }
            if (qty > 0) {
                cartListener.updateProductToCart(
                    data.productId,
                    data.variationId, qty
                )
            } else {
                cartListener.deleteCartProduct(
                    data.productId,
                    data.variationId
                )
            }

        }

        holder.rlProduct.setOnClickListener {
            mContext.startActivity(
                Intent(mContext, ProductDetailActivity::class.java)
                    .putExtra("productId", data.productId)
                    .putExtra("productName", data.productName)
            )
        }

    }

    override fun getItemCount(): Int {
        return nameList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvProductName = itemView.findViewById(R.id.tvProductName) as TextView
        val tvVariations = itemView.findViewById(R.id.tvVariations) as TextView
        val tvPrice = itemView.findViewById(R.id.tvPrice) as TextView
        val tvMrp = itemView.findViewById(R.id.tvMrp) as TextView
        val tvStock = itemView.findViewById(R.id.tvStock) as TextView
        val tvAddToCart = itemView.findViewById(R.id.tvAddToCart) as TextView
        val tvCartCount = itemView.findViewById(R.id.tvCartCount) as TextView
        val ivProduct = itemView.findViewById(R.id.ivProduct) as ImageView
        val ivPlusCart = itemView.findViewById(R.id.ivPlusCart) as ImageView
        val ivMinusCart = itemView.findViewById(R.id.ivMinusCart) as ImageView
        val ivArrow = itemView.findViewById(R.id.ivArrow) as ImageView
        val rlVariations = itemView.findViewById(R.id.rlVariations) as RelativeLayout
        val rlProduct = itemView.findViewById(R.id.rlProduct) as RelativeLayout
        val rlCArtAction = itemView.findViewById(R.id.rlCArtAction) as RelativeLayout
        val rlCartQuantity = itemView.findViewById(R.id.rlCartQuantity) as RelativeLayout
    }

    private fun dpToPx(dp: Int): Int {
        val r = mContext.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            r.displayMetrics
        ).toInt()
    }
}