package com.shriagrohitech.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.shriagrohitech.R
import com.shriagrohitech.constant.Constant
import com.shriagrohitech.models.cat_data.ProductVariation

class ProductVariationAdapter(
    private val variationList: ArrayList<ProductVariation>,
    private val mContext: Context,
    private val onClickListener: View.OnClickListener,
    private val selectP: Int
) : RecyclerView.Adapter<ProductVariationAdapter.ViewHolder>() {
    private val rowIndex = 0
    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        i: Int
    ): ViewHolder {
        val li = LayoutInflater.from(mContext)
        val view = li.inflate(R.layout.row_product_type, viewGroup, false)
        return ViewHolder(
            view
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val variation = variationList[i]
        viewHolder.rbProductPPrice.text =
            variation.stockPrice + Constant.slash + variation.variationQuantity + " " + variation.variation
        viewHolder.rbProductPPrice.isChecked = variation.isSelected
/*
        if (rowIndex == i) {
            viewHolder.rbProductPPrice.isChecked = true
            //  viewHolder.rbProductPPrice.setTextColor(mContext.resources.getColor(R.color.colorPrimary))

        } else {
            viewHolder.rbProductPPrice.isChecked = false
            viewHolder.rbProductPPrice.setTextColor(mContext.resources.getColor(R.color.black))
        }
*/
        viewHolder.rbProductPPrice.tag = i
        viewHolder.rbProductPPrice.setOnClickListener(onClickListener)/* {
            rowIndex == i
            notifyDataSetChanged()
            selectedItemPosition.selectedItem(i, "var")
        }*/

    }

    override fun getItemCount(): Int {
        return variationList.size
    }

    inner class ViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val rbProductPPrice: RadioButton = itemView.findViewById(R.id.rbProductPPrice)

    }

}