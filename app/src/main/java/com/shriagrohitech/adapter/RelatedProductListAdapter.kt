package com.shriagrohitech.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shriagrohitech.R
import com.shriagrohitech.models.cat_data.ProductDatum
import com.shriagrohitech.ui.ProductDetailActivity


class RelatedProductListAdapter(private val nameList: ArrayList<ProductDatum>, private val mContext: Context,
                                private val mActivity: Activity, private val from: String) :
    RecyclerView.Adapter<RelatedProductListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_home_cat, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data :ProductDatum = nameList[position]
        holder.tvCatTitle.text = data.productName
        Glide.with(mContext)
            .load(data.productImage)
            .centerCrop()
            .into(holder.ivCat)
        holder.rlImage.setOnClickListener{
            mContext.startActivity(
                Intent(mContext, ProductDetailActivity::class.java)
                    .putExtra("productId", data.productId)
                    .putExtra("productName", data.productName))
            if (from=="details") {
                mActivity.finish()
            }
        }
    }

    override fun getItemCount(): Int {
        return nameList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvCatTitle = itemView.findViewById(R.id.tvCatTitle) as TextView
        val ivCat = itemView.findViewById(R.id.ivCat) as ImageView
        val rlImage = itemView.findViewById(R.id.rlImage) as RelativeLayout
    }
}