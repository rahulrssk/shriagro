package com.shriagrohitech.adapter

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shriagrohitech.R
import com.shriagrohitech.models.home.Blog
import com.shriagrohitech.ui.BlogDetailActivity

class HomeBlogListAdapter(private val mContext: Context, private val blogs: ArrayList<Blog>) :
    RecyclerView.Adapter<HomeBlogListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_home_blog, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val blog = blogs[position]
        holder.tvBlogTitle.text = blog.title
        Glide.with(mContext)
            .load(blog.image)
            .placeholder(R.drawable.icf_leaves)
            .error(R.drawable.icf_leaves)
            .into(holder.ivBlog)
        holder.rlImage.setOnClickListener {
            mContext.startActivity(Intent(mContext, BlogDetailActivity::class.java)
                .putExtra("data", blog))
        }

        val lastPos = 4
        val params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )//params.setMargins(LTRB)
        if (position == 0) {
            params.setMargins(dpToPx(16), dpToPx(8), dpToPx(16), dpToPx(8))
        } else if (position == lastPos) {
            params.setMargins(dpToPx(16), dpToPx(16), dpToPx(16), dpToPx(16))
        }else{
            params.setMargins(dpToPx(16), dpToPx(16), dpToPx(16), dpToPx(8))
        }
        holder.rlRoot.layoutParams = params
    }

    override fun getItemCount(): Int {
        return blogs.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rlImage = itemView.findViewById(R.id.rlImage) as RelativeLayout
        val tvBlogTitle = itemView.findViewById(R.id.tvBlogTitle) as TextView
        val ivBlog = itemView.findViewById(R.id.ivBlog) as ImageView
        val rlRoot = itemView.findViewById(R.id.rlRoot) as RelativeLayout
    }

    private fun dpToPx(dp: Int): Int {
        val r: Resources = mContext.getResources()
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dp.toFloat(),
            r.displayMetrics
        ).toInt()
    }
}